cmake_minimum_required(VERSION 3.26)

if(CMAKE_SOURCE_DIR STREQUAL CMAKE_BINARY_DIR)
    message(FATAL_ERROR "Do not build in-source. Please remove CMakeCache.txt and the CMakeFiles/ directory. Then build out-of-source.")
endif()

project("substation" VERSION 0.0.1 LANGUAGES C CXX)

set(CMAKE_CXX_STANDARD 23)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

add_compile_definitions(-DBUILD_QT=ON -DBUILD_TESTS=ON -DBUILD_BENCHMARKS=ON)

include(CTest)
include(CMakeDependentOption)
include(GNUInstallDirs)
include(InstallRequiredSystemLibraries)
include(CMakePackageConfigHelpers)
include(FetchContent)

find_package(cpr 1.11.1 REQUIRED)
find_package(nlohmann_json 3.11.3 REQUIRED)
find_package(spdlog 1.8.5 REQUIRED)

if (BUILD_TESTS)
    find_package(catch2 3.7.1 REQUIRED)
endif()

if (BUILD_QT OR BUILD_BENCHMARKS)
    find_package(Qt6 6.4 REQUIRED COMPONENTS Core Quick Charts Test)
    qt_standard_project_setup()
endif()

set(CONFIG_INSTALL_DIR "${CMAKE_INSTALL_LIBDIR}/cmake/substation")
set(PACKAGE_CONFIG_INSTALL_DIR "${CMAKE_INSTALL_LIBDIR}/cmake/substation")

# Generate substationConfig.cmake using a template.
configure_package_config_file(
    "${CMAKE_CURRENT_SOURCE_DIR}/cmake/substation-config.cmake.in"
    "${CMAKE_CURRENT_BINARY_DIR}/substation-config.cmake"
    INSTALL_DESTINATION "${PACKAGE_CONFIG_INSTALL_DIR}"
    NO_SET_AND_CHECK_MACRO
    NO_CHECK_REQUIRED_COMPONENTS_MACRO
)

# Generate a version file.
write_basic_package_version_file(
    "${CMAKE_CURRENT_BINARY_DIR}/substation-configversion.cmake"
    VERSION ${PROJECT_VERSION}
    COMPATIBILITY SameMajorVersion
)

# Install both files.
install(
    FILES
        "${CMAKE_CURRENT_BINARY_DIR}/substation-config.cmake"
        "${CMAKE_CURRENT_BINARY_DIR}/substation-configversion.cmake"
    DESTINATION "${PACKAGE_CONFIG_INSTALL_DIR}"
)

set(LIBSUBSTATION_PREFIX "libsubstation-")
add_subdirectory("src")

if (BUILD_BENCHMARKS)
    add_subdirectory("benchmarks")
endif()