add_executable(task-monitor-tests taskmonitortests.cpp)
target_link_libraries(task-monitor-tests PRIVATE carbon-minimisation Catch2::Catch2WithMain)
add_test(NAME TaskMonitorTests COMMAND task-monitor-tests)

add_executable(task-scheduler-tests taskschedulertests.cpp)
target_link_libraries(task-scheduler-tests PRIVATE carbon-minimisation Catch2::Catch2WithMain)
add_test(NAME TaskSchedulerTests COMMAND task-scheduler-tests)