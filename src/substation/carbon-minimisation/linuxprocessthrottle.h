//
// Created by Claudio Cambra on 14/7/24.
//

#ifndef SUBSTATION_LINUXPROCESSTHROTTLE_H
#define SUBSTATION_LINUXPROCESSTHROTTLE_H

#include "processthrottle.h"

namespace Substation::CarbonMinimisation::Throttling {
    class LinuxProcessThrottle : public ProcessThrottle {
    public:
        void set_cpu_percent_limit(pid_t pid, int percentage) override;
        void set_memory_limit(pid_t pid, size_t limit) override;
        void set_priority(pid_t pid, int priority) override;
    };
}

#endif //SUBSTATION_LINUXPROCESSTHROTTLE_H
