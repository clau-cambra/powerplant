//
// Created by Claudio Cambra on 20/1/24.
//

#pragma once

#include <optional>
#include <sys/types.h>

namespace Substation::CarbonMinimisation::Execution
{

struct ThrottleDescriptor {
    std::optional<ushort> max_cpu_limit_percent;
    std::optional<ushort> min_cpu_limit_percent;
    std::optional<size_t> mem_limit_size;
    std::optional<int> process_priority;
};

}