//
// Created by Claudio Cambra on 14/7/24.
//

#include "macosprocessthrottle.h"

#include <array>
#include <mach/mach.h>
#include <mach/mach_host.h>
#include <sys/resource.h>
#include <sys/sysctl.h>
#include <sys/time.h>
#include <unistd.h>

#include <spdlog/spdlog.h>

namespace
{
// We have no levers for setting a specific CPU usage percentage on macOS; we can only set a task policy to make
// the process a given role, and this is really fuzzy.
task_role role_for_percentage_cpu_usage(const int percentage)
{
    if (percentage <= 50) {
        return TASK_FOREGROUND_APPLICATION;
    } else if (percentage <= 75) {
        return TASK_BACKGROUND_APPLICATION;
    } else {
        return TASK_CONTROL_APPLICATION;
    }
}

uint64_t physical_memory()
{
    std::array<int, 2> mib = {CTL_HW, HW_MEMSIZE};
    uint64_t memSize;

    if (auto len = sizeof(memSize); ::sysctl(mib.data(), 2, &memSize, &len, nullptr, 0) == -1) {
        spdlog::error("sysctl error: {}", ::strerror(errno));
        return 0;
    }

    return memSize;
}

// Same situation for memory usage
task_role role_for_memory_limit(const size_t usage)
{
    const auto memory = physical_memory();
    const auto limit_pc = (usage * 100) / std::max(memory, 1ULL);

    if (limit_pc <= 50) {
        return TASK_BACKGROUND_APPLICATION;
    } else if (limit_pc <= 75) {
        return TASK_DEFAULT_APPLICATION;
    } else {
        return TASK_CONTROL_APPLICATION;
    }
}

kern_return_t set_pid_task_role(const pid_t pid, const task_role role)
{
    mach_port_t task;

    if (const auto pid_task_result = ::task_for_pid(mach_task_self(), pid, &task); pid_task_result != KERN_SUCCESS) {
        spdlog::error("Failed to get task for pid: {}", ::mach_error_string(pid_task_result));
        return pid_task_result;
    }

    const task_category_policy_data_t policy{.role = role};
    const auto kr = ::task_policy_set(task, TASK_CATEGORY_POLICY, (task_policy_t)&policy, TASK_CATEGORY_POLICY_COUNT);
    if (kr != KERN_SUCCESS) {
        spdlog::error("Failed to set task policy: {}", ::mach_error_string(kr));
    }

    return kr;
}
}

namespace Substation::CarbonMinimisation::Throttling
{
void MacOSProcessThrottle::set_cpu_percent_limit(const pid_t pid, const int percentage)
{
    const auto role = role_for_percentage_cpu_usage(percentage);
    if (set_pid_task_role(pid, role) != KERN_SUCCESS) {
        spdlog::error("Failed to set CPU limit: {}", pid);
    }
}

void MacOSProcessThrottle::set_memory_limit(const pid_t pid, const size_t limit)
{
    // On macOS we can use setrlimit to set the memory limit for the current process, but:
    // - we can only set the limit for the stack (RLIMIT_DATA and RLIMIT_AS are ignored)
    // - we can't do it for other processes and can only set a task policy to make the process a given role (and this is really fuzzy).
    // Due to this we can only really depend on the task role to set the memory limit for a process.
    const auto role = role_for_memory_limit(limit);
    if (set_pid_task_role(pid, role) != KERN_SUCCESS) {
        spdlog::error("Failed to set memory limit for PID: {}", pid);
    }
}

void MacOSProcessThrottle::set_priority(const pid_t pid, const int priority)
{
    if (::setpriority(PRIO_PROCESS, pid, priority) != 0) {
        spdlog::error("Failed to set priority: {}", ::strerror(errno));
    }
}
} // Substation::Throttling