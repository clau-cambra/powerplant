//
// Created by Claudio Cambra on 14/7/24.
//

#ifndef SUBSTATION_CARBON_MINIMISATION_UTILS_H
#define SUBSTATION_CARBON_MINIMISATION_UTILS_H

namespace Substation::Throttling
{
class ProcessThrottle;

ProcessThrottle *createProcessThrottle();
};

#endif // SUBSTATION_CARBON_MINIMISATION_UTILS_H
