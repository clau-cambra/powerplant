//
// Created by Claudio Cambra on 24/1/25.
//

#ifndef SUBSTATION_TASKMONITOR_H
#define SUBSTATION_TASKMONITOR_H

#include <chrono>
#include <memory>
#include <thread>

#include "iteratingtaskdescriptor.h"

#include <substation/carbon-modelling/carbonintensitycurve.h>

namespace Substation
{
namespace CarbonMinimisation::Execution
{

int64_t get_thread_cpu_time(const pthread_t thread_id);

class TaskMonitor
{
public:
    explicit TaskMonitor(const IteratingTaskDescriptor &task_desc,
                         const std::shared_ptr<CarbonModelling::CarbonIntensityCurve> &intensity_curve = {},
                         const std::function<void(std::chrono::seconds)> &duration_update_callback = {},
                         const std::function<void(CarbonModelling::CarbonIntensityCurve::Period)> &lowest_intensity_period_update_callback = {},
                         const std::function<void(float)> &applied_cpu_limit_callback = {},
                         const std::function<void(int)> &iteration_completed_callback = {});
    ~TaskMonitor();

    void start();
    void stop();

    void set_lowest_intensity_period_callback(const std::function<void(CarbonModelling::CarbonIntensityCurve::Period)> &callback)
    {
        m_lowest_intensity_period_update_callback = callback;
    }
    void set_duration_update_callback(const std::function<void(std::chrono::seconds)> &callback)
    {
        m_duration_update_callback = callback;
    }
    void set_applied_cpu_limit_callback(const std::function<void(float)> &callback)
    {
        m_applied_cpu_limit_callback = callback;
    }
    void set_iteration_completed_callback(const std::function<void(int)> &callback)
    {
        m_iteration_completed_callback = callback;
    }

    bool is_running() const
    {
        return m_running;
    }

    std::shared_ptr<const CarbonModelling::CarbonIntensityCurve> get_carbon_curve() const
    {
        return std::const_pointer_cast<CarbonModelling::CarbonIntensityCurve>(m_carbon_curve);
    }

    std::optional<CarbonModelling::CarbonIntensityCurve::Period> get_lowest_intensity_period() const
    {
        return m_lowest_intensity_period;
    }

    int get_completed_iters() const
    {
        return m_completed_iters;
    }

    std::chrono::nanoseconds estimated_time_per_iter() const
    {
        return m_estimated_time_per_iter;
    }

private:
    void perform_task_with_throttling();

    std::thread m_monitor_thread;
    std::atomic<bool> m_running = true;
    int m_completed_iters = 0;
    std::chrono::nanoseconds m_estimated_time_per_iter = std::chrono::nanoseconds::zero();
    std::optional<CarbonModelling::CarbonIntensityCurve::Period> m_lowest_intensity_period;
    const IteratingTaskDescriptor m_task_desc;
    const std::shared_ptr<CarbonModelling::CarbonIntensityCurve> m_carbon_curve;
    std::function<void(std::chrono::seconds)> m_duration_update_callback;
    std::function<void(CarbonModelling::CarbonIntensityCurve::Period)> m_lowest_intensity_period_update_callback;
    std::function<void(float)> m_applied_cpu_limit_callback;
    std::function<void(int)> m_iteration_completed_callback;
};

}
}

#endif // SUBSTATION_TASKMONITOR_H