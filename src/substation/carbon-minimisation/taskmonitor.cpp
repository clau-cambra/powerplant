//
// Created by Claudio Cambra on 24/1/25.
//

#include "taskmonitor.h"

#include <cassert>
#include <chrono>
#include <iostream>
#include <mach/mach.h>
#include <pthread.h>

#include <substation/common/eventloop.h>

namespace Substation::CarbonMinimisation::Execution
{

#if defined(__APPLE__)
int64_t get_thread_cpu_time_apple(const pthread_t thread_id)
{
    const auto mach_thread = ::pthread_mach_thread_np(thread_id);
    thread_basic_info_data_t info;
    auto count = THREAD_BASIC_INFO_COUNT;
    ::thread_info(mach_thread, THREAD_BASIC_INFO, (thread_info_t)&info, &count);
    const auto user_time = info.user_time.seconds * 1000000000LL + info.user_time.microseconds * 1000LL;
    const auto system_time = info.system_time.seconds * 1000000000LL + info.system_time.microseconds * 1000LL;
    return user_time + system_time;
}
#elif defined(__linux__)
int64_t get_thread_cpu_time_linux(const pthread_t thread_id)
{
    clockid_t cid;
    ::pthread_getcpuclockid(thread_id, &cid);
    timespec ts;
    ::clock_gettime(cid, &ts);
    return ts.tv_sec * 1000000000LL + ts.tv_nsec;
}
#endif

int64_t get_thread_cpu_time(const pthread_t thread_id)
{
#if defined(__APPLE__)
    return get_thread_cpu_time_apple(thread_id);
#elif defined(__linux__)
    return get_thread_cpu_time_linux(thread_id);
#else
#error "Unsupported platform"
#endif
}

TaskMonitor::TaskMonitor(const IteratingTaskDescriptor &task_desc,
                         const std::shared_ptr<CarbonModelling::CarbonIntensityCurve> &intensity_curve,
                         const std::function<void(std::chrono::seconds)> &duration_update_callback,
                         const std::function<void(CarbonModelling::CarbonIntensityCurve::Period)> &lowest_intensity_period_update_callback,
                         const std::function<void(float)> &applied_cpu_limit_callback,
                         const std::function<void(int)> &iteration_completed_callback)
    : m_task_desc(task_desc)
    , m_carbon_curve(intensity_curve)
    , m_duration_update_callback(duration_update_callback)
    , m_lowest_intensity_period_update_callback(lowest_intensity_period_update_callback)
    , m_applied_cpu_limit_callback(applied_cpu_limit_callback)
    , m_iteration_completed_callback(iteration_completed_callback)
{
}

TaskMonitor::~TaskMonitor()
{
    stop();
}

void TaskMonitor::start()
{
    std::shared_ptr<void> _(nullptr, [this](const auto) { // defer-like
        m_running = false;
    });
    if (!m_task_desc.throttle_desc.has_value() || !m_task_desc.throttle_desc->max_cpu_limit_percent.has_value()) {
        return;
    }
    m_monitor_thread = std::thread(&TaskMonitor::perform_task_with_throttling, this);
}

void TaskMonitor::stop()
{
    m_running.store(false);
    if (m_monitor_thread.joinable()) {
        m_monitor_thread.join();
    }
}

void TaskMonitor::perform_task_with_throttling()
{
    m_running.store(true);

    // Add a safety margin to ensure we stay under the limit
    constexpr auto SAFETY_MARGIN = 0.98;
    constexpr auto MAX_POSSIBLE_CPU_LIMIT = 100;
    constexpr auto MIN_POSSIBLE_CPU_LIMIT = 1;

    const auto max_cpu_limit = m_task_desc.throttle_desc->max_cpu_limit_percent.value_or(MAX_POSSIBLE_CPU_LIMIT);
    const auto min_cpu_limit = m_task_desc.throttle_desc->min_cpu_limit_percent.value_or(MIN_POSSIBLE_CPU_LIMIT);
    const auto clamped_max_cpu_limit = std::clamp(max_cpu_limit, min_cpu_limit, static_cast<unsigned short>(MAX_POSSIBLE_CPU_LIMIT));
    const auto clamped_min_cpu_limit = std::clamp(min_cpu_limit, static_cast<unsigned short>(MIN_POSSIBLE_CPU_LIMIT), max_cpu_limit);
    const auto max_cpu_limit_with_margin = clamped_max_cpu_limit * SAFETY_MARGIN;
    double applied_cpu_limit = max_cpu_limit_with_margin;

    using namespace std::chrono;
    int64_t last_cpu_time = 0;
    auto last_wall_time = steady_clock::now();

    // Initialize average tracking
    double total_cpu_usage = 0.0;
    size_t sample_count = 0;

    // Initialize duration tracking
    const auto start_time = system_clock::now();
    const auto have_expected_iters = m_task_desc.expected_iters.has_value();

    const auto thread_id = ::pthread_self();
    while (m_running && m_task_desc.task_iter().has_value()) {
        ++m_completed_iters;

        try {
            const auto current_cpu_time = get_thread_cpu_time(thread_id);
            const auto current_wall_time = steady_clock::now();

            const auto cpu_time_delta = current_cpu_time - last_cpu_time;
            const auto wall_time_delta = duration_cast<nanoseconds>(current_wall_time - last_wall_time).count();

            // Calculate current CPU usage percentage
            const auto cpu_usage = 100.0 * static_cast<double>(cpu_time_delta) / static_cast<double>(wall_time_delta);

            // Update simple average
            sample_count++;
            total_cpu_usage += cpu_usage;
            if (const auto avg_cpu_usage = total_cpu_usage / static_cast<double>(sample_count); avg_cpu_usage > applied_cpu_limit) {
                // Simple duty cycle approach: for every unit of CPU time,
                // sleep for enough time to achieve desired CPU percentage
                // Example: For 50% CPU limit, sleep for equal amount of time as CPU time
                const long double target_ratio = (100.0 - applied_cpu_limit) / applied_cpu_limit;
                const auto sleep_duration = nanoseconds(static_cast<int64_t>(cpu_time_delta * target_ratio));
                std::this_thread::sleep_for(sleep_duration);
            }

            last_cpu_time = current_cpu_time;
            last_wall_time = current_wall_time;

            if (have_expected_iters) {
                // Update remaining duration
                const auto current_time = system_clock::now();
                const auto elapsed_time = duration_cast<nanoseconds>(current_time - start_time);
                m_estimated_time_per_iter = nanoseconds(static_cast<long>(std::ceil(elapsed_time.count() / static_cast<double>(m_completed_iters))));

                const auto total_duration = duration_cast<seconds>(m_estimated_time_per_iter * *m_task_desc.expected_iters);
                if (m_duration_update_callback) {
                    Common::EventLoop::get_instance()->execute([this, total_duration] {
                        m_duration_update_callback(total_duration);
                    });
                }
                if (m_carbon_curve != nullptr) {
                    const auto start_time_t = system_clock::to_time_t(start_time);
                    const auto duration_time_t = static_cast<std::time_t>(total_duration.count());
                    const auto search_end_time_t = m_carbon_curve->size() > 0
                        ? std::max(start_time_t + duration_time_t, ((*m_carbon_curve)[m_carbon_curve->size()]).datetime)
                        : start_time_t + duration_time_t;
                    const auto lowest_intensity_period_expect =
                        m_carbon_curve->lowest_intensity_period(start_time_t,
                                                                search_end_time_t,
                                                                duration_time_t,
                                                                CarbonModelling::CarbonIntensityCurve::any_use_past_24h);
                    if (lowest_intensity_period_expect.has_value() && *lowest_intensity_period_expect != m_lowest_intensity_period) {
                        m_lowest_intensity_period = *lowest_intensity_period_expect;
                        if (m_lowest_intensity_period_update_callback) {
                            Common::EventLoop::get_instance()->execute([this, lowest_intensity_period_expect] {
                                m_lowest_intensity_period_update_callback(*lowest_intensity_period_expect);
                            });
                        }
                    }
                }
            }

        } catch (const std::system_error &e) {
            std::cerr << "Error getting thread CPU time: " << e.what() << std::endl;
            break;
        }

        // Given the current expected duration of an iteration, calculate what the intensity is
        // going to be between now and the completion of the next iteration and throttle accordingly
        // based on the relation between this intensity the lowest intensity period min/max intensities
        if (have_expected_iters && m_lowest_intensity_period.has_value() && m_carbon_curve != nullptr) {
            const auto &lip_val = *m_lowest_intensity_period;
            const auto lowest_intensity = lip_val.intensities[lip_val.min_intensity_index].intensity;
            const auto highest_intensity = lip_val.intensities[lip_val.max_intensity_index].intensity;

            const auto now = system_clock::now();
            const auto iter_expected_finish = now + duration_cast<seconds>(m_estimated_time_per_iter);
            const auto now_intensity = m_carbon_curve->intensity_at(system_clock::to_time_t(now), CarbonModelling::CarbonIntensityCurve::any_use_past_24h);
            const auto iter_finish_intensity =
                m_carbon_curve->intensity_at(system_clock::to_time_t(iter_expected_finish), CarbonModelling::CarbonIntensityCurve::any_use_past_24h);

            if (now_intensity.has_value() && iter_finish_intensity.has_value()) {
                const auto old_limit = applied_cpu_limit;
                const auto average_iter_intensity = (now_intensity->intensity + iter_finish_intensity->intensity) / 2.0;
                // Main idea: lowest intensity == maximum cpu usage limit, highest intensity == minimum cpu usage limit
                // Use what the average intensity at iteration completion time is as a ratio between the lowest and highest intensity.
                // Then, use this to figure out what cpu limit value to apply between the min and the max limits.

                // Step 1: create a ratio between 0 and 1 that represents where the current intensity falls between the lowest and highest intensities:
                const auto intensity_ratio = (average_iter_intensity - lowest_intensity) / static_cast<double>(highest_intensity - lowest_intensity);
                // Step 2: use the ratio to calculate the cpu limit to apply between the min and the max limits (inverted):
                applied_cpu_limit = max_cpu_limit_with_margin - (max_cpu_limit_with_margin - clamped_min_cpu_limit) * intensity_ratio;

                if (m_applied_cpu_limit_callback && applied_cpu_limit != old_limit) {
                    m_applied_cpu_limit_callback(applied_cpu_limit);
                }
            }
        }

        if (m_iteration_completed_callback) {
            m_iteration_completed_callback(m_completed_iters);
        }
    }
}

}