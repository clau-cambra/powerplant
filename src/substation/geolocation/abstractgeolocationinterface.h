//
// Created by Claudio Cambra on 25/12/24. Merry Christmas!
//

#pragma once

#include <future>

#include "geolocation.h"

namespace Substation::Geolocation
{

class AbstractGeolocationInterface
{
public:
    virtual ~AbstractGeolocationInterface() = default;
    virtual std::future<std::optional<Geolocation>> get_geolocation() const = 0;
};

}