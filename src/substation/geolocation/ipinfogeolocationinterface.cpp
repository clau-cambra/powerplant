//
// Created by Claudio Cambra on 25/12/24. Merry Christmas!
//

#include "ipinfogeolocationinterface.h"

#include <iostream>

#include <cpr/api.h>
#include <cpr/cprtypes.h>

#include <nlohmann/json.hpp>

#include <spdlog/spdlog.h>

namespace Substation::Geolocation
{

IpInfoGeolocationInterface::IpInfoGeolocationInterface(const std::string &ip_info_token)
    : m_ip_info_token(ip_info_token)
{
}

std::future<std::optional<Geolocation>> IpInfoGeolocationInterface::get_geolocation() const
{
    return std::async(std::launch::async, [this] noexcept -> std::optional<Geolocation> {
        const auto response = cpr::Get(cpr::Url{"https://ipinfo.io"}, cpr::Header{{"Authorization", "Bearer " + m_ip_info_token}});
        if (response.error.code != cpr::ErrorCode::OK) {
            spdlog::error("Failed to get geolocation, received response code: {}", response.status_code);
            return std::nullopt;
        }
        spdlog::info("Successful ipinfo geolocation response!");

        try {
            const auto json = nlohmann::json::parse(response.text);
            return Geolocation{
                .name = json.at("city").get<std::string>(),
                .latitude = std::stof(json.at("loc").get<std::string>().substr(0, json.at("loc").get<std::string>().find(','))),
                .longitude = std::stof(json.at("loc").get<std::string>().substr(json.at("loc").get<std::string>().find(',') + 1)),
                .region = json.at("region").get<std::string>(),
                .country = json.at("country").get<std::string>(),
                .timezone = json.at("timezone").get<std::string>()
            };
        } catch (nlohmann::json::exception &e) {
            spdlog::error("Failed to parse geolocation response: {}", e.what());
            return std::nullopt;
        } catch (...) {
            return std::nullopt;
        }
    });
}

}