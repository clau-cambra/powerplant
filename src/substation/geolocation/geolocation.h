//
// Created by Claudio Cambra on 25/12/24. Merry Christmas!
//

#pragma once

#include <string>

namespace Substation::Geolocation
{
struct Geolocation {
    std::string name;
    float latitude;
    float longitude;
    std::string region;
    std::string country;
    std::string timezone;

    bool operator==(const Geolocation &other) const = default;
};
}