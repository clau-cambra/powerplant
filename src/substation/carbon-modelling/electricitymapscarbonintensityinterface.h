//
// Created by Claudio Cambra on 25/12/24. Merry Christmas!
//

#pragma once

#include "abstractcarbonintensityinterface.h"

namespace Substation::CarbonModelling
{

struct Geolocation;

class ElectricityMapCarbonIntensityInterface : public AbstractCarbonIntensityInterface
{
public:
    explicit ElectricityMapCarbonIntensityInterface(const std::string &api_key);
    ~ElectricityMapCarbonIntensityInterface() override = default;

    std::future<std::optional<CarbonIntensity>> get_carbon_intensity(const Substation::Geolocation::Geolocation &geolocation) const override;

private:
    std::string m_api_key;
};

}