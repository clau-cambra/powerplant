//
// Created by Claudio Cambra on 26/2/25.
//

#include <substation/carbon-modelling/abstractcarbonintensityinterface.h>
#include <substation/carbon-modelling/carbonintensitymonitor.h>
#include <substation/geolocation/abstractgeolocationinterface.h>
#include <substation/geolocation/geolocation.h>

namespace Substation::CarbonModelling::TestUtils
{
constexpr auto MOCK_LOCATION_NAME = "Málaga";
constexpr auto TEST_GEOLOCATION = Substation::Geolocation::Geolocation{MOCK_LOCATION_NAME, 36.7213f, -4.4215f};
constexpr auto TEST_CARBON_INTENSITY = CarbonIntensity{"ES", 100, 1640601600, 1640601600, "unknown", false, "unknown"};

class MockGeolocationInterface : public Substation::Geolocation::AbstractGeolocationInterface
{
public:
    std::future<std::optional<Substation::Geolocation::Geolocation>> get_geolocation() const override
    {
        return std::async(std::launch::async, [] -> std::optional<Substation::Geolocation::Geolocation> {
            return TEST_GEOLOCATION;
        });
    }
};

class MockCarbonIntensityInterface : public AbstractCarbonIntensityInterface
{
public:
    std::future<std::optional<CarbonIntensity>> get_carbon_intensity(const Substation::Geolocation::Geolocation &location) const override
    {
        return std::async(std::launch::async, [&location] -> std::optional<CarbonIntensity> {
            if (location == TEST_GEOLOCATION) {
                return TEST_CARBON_INTENSITY;
            }
            return std::nullopt;
        });
    }
};

const auto gl_interface = std::make_shared<MockGeolocationInterface>();
const auto ci_interface = std::make_shared<MockCarbonIntensityInterface>();

}