//
// Created by Claudio Cambra on 22/1/25.
//

#include <catch2/catch_all.hpp>
#include <cstdlib>
#include <filesystem>

#include <substation/carbon-modelling/carbonintensity.h>
#include <substation/carbon-modelling/filebasedcarbonintensitystorage.h>

using namespace Substation::CarbonModelling;

namespace
{

class UtilsTestFixture
{
public:
    const std::filesystem::path test_ci_path;

    explicit UtilsTestFixture(const std::filesystem::path &test_path)
        : test_ci_path(test_path)
    {
        ::setenv(FileBasedCarbonIntensityStorage::STORE_ENV_VAR_NAME, test_path.c_str(), 1);
        if (const auto store_path = FileBasedCarbonIntensityStorage().store_folder_path(); !std::filesystem::exists(store_path)) {
            REQUIRE(std::filesystem::create_directories(store_path));
        }
    }

    ~UtilsTestFixture()
    {
        REQUIRE(std::filesystem::remove_all(test_ci_path));
        if (m_pre_test_var == nullptr) {
            ::unsetenv(FileBasedCarbonIntensityStorage::STORE_ENV_VAR_NAME);
            REQUIRE(std::getenv(FileBasedCarbonIntensityStorage::STORE_ENV_VAR_NAME) == nullptr);
        } else {
            ::setenv(FileBasedCarbonIntensityStorage::STORE_ENV_VAR_NAME, m_pre_test_var, 1);
            REQUIRE(strcmp(std::getenv(FileBasedCarbonIntensityStorage::STORE_ENV_VAR_NAME), m_pre_test_var) == 0);
        }
    }

private:
    const char *const m_pre_test_var = std::getenv(FileBasedCarbonIntensityStorage::STORE_ENV_VAR_NAME);
};

}

TEST_CASE("Carbon intensity store path from env var works correctly", "[utils-ci-env-store-path]")
{
    const auto test_path = std::filesystem::temp_directory_path() / "super_secret_test_dir";
    const auto test_path_str = test_path.c_str();
    const auto test_fixture = UtilsTestFixture(test_path);
    REQUIRE(strcmp(std::getenv(FileBasedCarbonIntensityStorage::STORE_ENV_VAR_NAME), test_path_str) == 0);
}

TEST_CASE("Fallback carbon intensity store path works correctly", "[utils-ci-noenv-store-path]")
{
    const auto pre_test_var = std::getenv(FileBasedCarbonIntensityStorage::STORE_ENV_VAR_NAME);
    if (pre_test_var) {
        ::unsetenv(FileBasedCarbonIntensityStorage::STORE_ENV_VAR_NAME);
    }

    const auto expected_path = std::filesystem::temp_directory_path().append("carbon_intensities");
    const auto received_path = FileBasedCarbonIntensityStorage().store_folder_path();

    if (pre_test_var) {
        REQUIRE(::setenv(FileBasedCarbonIntensityStorage::STORE_ENV_VAR_NAME, pre_test_var, 0));
    }

    REQUIRE(received_path == expected_path);
}

TEST_CASE("Intensity data retrieval works as expected", "[utils-ci-get]")
{
    const auto test_fixture = UtilsTestFixture(std::filesystem::temp_directory_path().append("utils-ci-get-test-dir"));
    auto file_storage = FileBasedCarbonIntensityStorage();

    const auto ci_0 = CarbonIntensity{.zone = "ES",
                                      .intensity = 200,
                                      .datetime = 1640601600,
                                      .updated_at = 1640601600,
                                      .emission_factor_type = "unknown",
                                      .is_estimated = false,
                                      .estimation_method = "unknown"};
    const auto ci_0_path = file_storage.store_folder_path().append("test_ci_0.json");
    REQUIRE(ci_0.write_to_file(ci_0_path));

    const auto ci_1 = CarbonIntensity{.zone = "ES",
                                      .intensity = 220,
                                      .datetime = 1640801600,
                                      .updated_at = 1640801600,
                                      .emission_factor_type = "unknown",
                                      .is_estimated = false,
                                      .estimation_method = "unknown"};
    const auto ci_1_path = file_storage.store_folder_path().append("test_ci_1.json");
    REQUIRE(ci_1.write_to_file(ci_1_path));

    const auto ci_2 = CarbonIntensity{.zone = "ES",
                                      .intensity = 240,
                                      .datetime = 1641001600,
                                      .updated_at = 1641001600,
                                      .emission_factor_type = "unknown",
                                      .is_estimated = false,
                                      .estimation_method = "unknown"};
    const auto ci_2_path = file_storage.store_folder_path().append("2.json");
    REQUIRE(ci_2.write_to_file(ci_2_path));

    const auto intensities = file_storage.data();
    REQUIRE(intensities.has_value());
    REQUIRE(intensities->size() == 3);
}
