//
// Created by Claudio Cambra on 06/02/25.
//

#include <catch2/catch_all.hpp>

#include "carbonmodellingtestutils.h"

using namespace Substation::CarbonModelling;

TEST_CASE("Carbon intensity monitor correctly fetches carbon intensity", "[carbon-intensity-monitor-fetch]")
{
    const auto monitor = CarbonIntensityMonitor::create(TestUtils::gl_interface, TestUtils::ci_interface);
    const auto carbon_intensity = monitor->fetch_carbon_intensity();
    REQUIRE(carbon_intensity.has_value());
    REQUIRE(carbon_intensity == TestUtils::TEST_CARBON_INTENSITY);
}

TEST_CASE("Carbon intensity monitor automatically retrieves carbon intensity", "[carbon-intensity-monitor-auto]")
{
    auto carbon_intensity_acquired = false;
    CarbonIntensity carbon_intensity;
    auto geolocation_acquired = false;
    Substation::Geolocation::Geolocation geolocation;
    auto monitor = CarbonIntensityMonitor::create(TestUtils::gl_interface,
                                                  TestUtils::ci_interface,
                                                  {},
                                                  {.intensity_fetched =
                                                       [&carbon_intensity_acquired, &carbon_intensity](const CarbonIntensity &ci) {
                                                           carbon_intensity_acquired = true;
                                                           carbon_intensity = ci;
                                                       },
                                                   .geolocation_fetched =
                                                       [&geolocation_acquired, &geolocation](const Substation::Geolocation::Geolocation &gl) {
                                                           geolocation_acquired = true;
                                                           geolocation = gl;
                                                       }},
                                                  std::chrono::seconds{1});

    monitor->start();
    std::this_thread::sleep_for(std::chrono::seconds(2));
    monitor->stop();

    REQUIRE(carbon_intensity_acquired);
    REQUIRE(carbon_intensity == TestUtils::TEST_CARBON_INTENSITY);

    REQUIRE(geolocation_acquired);
    REQUIRE(geolocation == TestUtils::TEST_GEOLOCATION);
}

TEST_CASE("Carbon intensity monitor correctly uses applied callbacks", "[carbon-intensity-callback-setters]")
{
    auto monitor = CarbonIntensityMonitor::create(TestUtils::gl_interface, TestUtils::ci_interface, {}, {}, std::chrono::seconds{1});
    auto carbon_intensity_acquired = false;
    CarbonIntensity carbon_intensity;
    auto geolocation_acquired = false;
    Substation::Geolocation::Geolocation geolocation;

    monitor->add_intensity_fetched_callback([&carbon_intensity_acquired, &carbon_intensity](const CarbonIntensity &ci) {
        carbon_intensity_acquired = true;
        carbon_intensity = ci;
    });
    monitor->add_geolocation_fetched_callback([&geolocation_acquired, &geolocation](const Substation::Geolocation::Geolocation &gl) {
        geolocation_acquired = true;
        geolocation = gl;
    });

    monitor->start();
    std::this_thread::sleep_for(std::chrono::seconds(2));
    monitor->stop();

    REQUIRE(carbon_intensity_acquired);
    REQUIRE(carbon_intensity == TestUtils::TEST_CARBON_INTENSITY);

    REQUIRE(geolocation_acquired);
    REQUIRE(geolocation == TestUtils::TEST_GEOLOCATION);
}

TEST_CASE("Carbon intensity monitor correctly starts/stops", "[carbon-intensity-monitor-startstop]")
{
    auto carbon_intensity_acquired = false;
    auto monitor = CarbonIntensityMonitor::create(TestUtils::gl_interface,
                                                  TestUtils::ci_interface,
                                                  {},
                                                  {.intensity_fetched =
                                                       [&carbon_intensity_acquired](const CarbonIntensity &) {
                                                           carbon_intensity_acquired = true;
                                                       }},
                                                  std::chrono::duration_cast<std::chrono::minutes>(std::chrono::seconds{1}));

    monitor->start();
    std::this_thread::sleep_for(std::chrono::seconds(2));
    REQUIRE(carbon_intensity_acquired);

    monitor->stop();
    std::this_thread::sleep_for(std::chrono::seconds(1));
    carbon_intensity_acquired = false;
    std::this_thread::sleep_for(std::chrono::seconds(2));
    REQUIRE(!carbon_intensity_acquired);
}

TEST_CASE("Carbon intensity monitor correctly responds to interval changes", "[carbon-intensity-monitor-interval]")
{
    std::atomic_int carbon_intensity_acquired_counter = 0;
    auto monitor = CarbonIntensityMonitor::create(TestUtils::gl_interface,
                                                  TestUtils::ci_interface,
                                                  {},
                                                  {.intensity_fetched =
                                                       [&carbon_intensity_acquired_counter](const CarbonIntensity &) {
                                                           carbon_intensity_acquired_counter.store(carbon_intensity_acquired_counter.load() + 1);
                                                       }},
                                                  std::chrono::seconds{1});

    monitor->start();
    std::this_thread::sleep_for(std::chrono::seconds(2));
    REQUIRE(carbon_intensity_acquired_counter.load() > 0);

    monitor->set_interval(std::chrono::seconds{3});
    carbon_intensity_acquired_counter = 0;
    std::this_thread::sleep_for(std::chrono::seconds(5));
    REQUIRE(carbon_intensity_acquired_counter.load() < 2);
}