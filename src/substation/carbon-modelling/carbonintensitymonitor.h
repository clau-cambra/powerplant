//
// Created by Claudio Cambra on 13/2/25.
//

#pragma once

#include <atomic>
#include <chrono>
#include <expected>

#include <substation/geolocation/abstractgeolocationinterface.h>

#include "abstractcarbonintensityinterface.h"
#include "abstractcarbonintensitystorage.h"
#include "carbonintensity.h"

namespace Substation::CarbonModelling
{
class CarbonIntensityMonitor : public std::enable_shared_from_this<CarbonIntensityMonitor>
{
public:
    enum class Error {
        no_error,
        geolocation_fetch_error,
        carbon_intensity_fetch_error,
        store_error,
        monitor_stopped_error
    };

    struct Callbacks {
        std::function<void()> intensity_fetch_started;
        std::function<void(const CarbonIntensity &)> intensity_fetched;
        std::function<void()> geolocation_fetch_started;
        std::function<void(const Substation::Geolocation::Geolocation &)> geolocation_fetched;
    };

    ~CarbonIntensityMonitor() = default;

    static std::shared_ptr<CarbonIntensityMonitor> create(const std::shared_ptr<Substation::Geolocation::AbstractGeolocationInterface> &gl_interface,
                                                          const std::shared_ptr<AbstractCarbonIntensityInterface> &ci_interface,
                                                          const std::shared_ptr<AbstractCarbonIntensityStorage> &storage = {},
                                                          const Callbacks &callbacks = {},
                                                          const std::chrono::seconds interval = DEFAULT_INTERVAL);

    void start();
    void stop();
    void fetch();

    bool running() const
    {
        return m_running.load();
    };

    std::chrono::seconds interval() const
    {
        return m_interval.load();
    };
    void set_interval(const std::chrono::seconds interval)
    {
        m_interval.store(interval);
    };

    void add_intensity_fetch_started_callback(const std::function<void()> &intensity_fetch_started_callback)
    {
        std::scoped_lock lock(m_callbacks_mutex);
        if (intensity_fetch_started_callback) {
            m_intensity_fetch_started_callbacks.push_back(intensity_fetch_started_callback);
        }
    };
    void add_intensity_fetched_callback(const std::function<void(const CarbonIntensity &)> &intensity_fetched_callback)
    {
        std::scoped_lock lock(m_callbacks_mutex);
        if (intensity_fetched_callback) {
            m_intensity_fetched_callbacks.push_back(intensity_fetched_callback);
        }
    };
    void add_geolocation_fetch_started_callback(const std::function<void()> &geolocation_fetch_started_callback)
    {
        std::scoped_lock lock(m_callbacks_mutex);
        if (geolocation_fetch_started_callback) {
            m_geolocation_fetch_started_callbacks.push_back(geolocation_fetch_started_callback);
        }
    }
    void add_geolocation_fetched_callback(const std::function<void(const Substation::Geolocation::Geolocation &)> &geolocation_fetched_callback)
    {
        std::scoped_lock lock(m_callbacks_mutex);
        if (geolocation_fetched_callback) {
            m_geolocation_fetched_callbacks.push_back(geolocation_fetched_callback);
        }
    };
    void add_callbacks(const Callbacks &callbacks)
    {
        add_intensity_fetch_started_callback(callbacks.intensity_fetch_started);
        add_intensity_fetched_callback(callbacks.intensity_fetched);
        add_geolocation_fetch_started_callback(callbacks.geolocation_fetch_started);
        add_geolocation_fetched_callback(callbacks.geolocation_fetched);
    };

    std::expected<CarbonIntensity, Error> fetch_carbon_intensity();

private:
    static constexpr auto DEFAULT_INTERVAL = std::chrono::duration_cast<std::chrono::seconds>(std::chrono::minutes{15});
    static constexpr auto INTERNAL_CHECK_INTERVAL = std::chrono::seconds{1};

    explicit CarbonIntensityMonitor(const std::shared_ptr<Substation::Geolocation::AbstractGeolocationInterface> &gl_interface,
                                    const std::shared_ptr<AbstractCarbonIntensityInterface> &ci_interface,
                                    const std::shared_ptr<AbstractCarbonIntensityStorage> &storage,
                                    const Callbacks &callbacks,
                                    const std::chrono::seconds interval);

    void monitor();

    std::shared_ptr<Substation::Geolocation::AbstractGeolocationInterface> m_gl_interface;
    std::shared_ptr<AbstractCarbonIntensityInterface> m_ci_interface;
    std::shared_ptr<AbstractCarbonIntensityStorage> m_ci_storage;

    std::mutex m_callbacks_mutex;
    std::vector<std::function<void()>> m_intensity_fetch_started_callbacks;
    std::vector<std::function<void(const CarbonIntensity &)>> m_intensity_fetched_callbacks;
    std::vector<std::function<void()>> m_geolocation_fetch_started_callbacks;
    std::vector<std::function<void(const Substation::Geolocation::Geolocation &)>> m_geolocation_fetched_callbacks;
    
    std::atomic<std::chrono::seconds> m_interval = DEFAULT_INTERVAL;
    std::atomic_bool m_running{false};
    std::atomic<std::chrono::system_clock::time_point> m_last_check = std::chrono::system_clock::time_point::min();
};
}