//
// Created by Claudio Cambra on 25/12/24. Merry Christmas!
//

#pragma once

#include <ctime>
#include <filesystem>
#include <nlohmann/json.hpp>
#include <string>

namespace Substation::CarbonModelling
{

struct CarbonIntensity {
    std::string zone;
    int intensity;
    std::time_t datetime;
    std::time_t updated_at;
    std::string emission_factor_type;
    bool is_estimated;
    std::string estimation_method;
    bool is_forecast;
    std::string forecast_method;
    bool is_synthetic;

    NLOHMANN_DEFINE_TYPE_INTRUSIVE(CarbonIntensity,
                                   zone,
                                   intensity,
                                   datetime,
                                   updated_at,
                                   emission_factor_type,
                                   is_estimated,
                                   estimation_method,
                                   is_forecast,
                                   forecast_method);

    static std::optional<CarbonIntensity> from_file(const std::filesystem::path &path) noexcept;
    static std::vector<CarbonIntensity> from_files(const std::filesystem::path &store_path);
    static CarbonIntensity interpolate(const CarbonIntensity &prev, const CarbonIntensity &next, std::time_t time);
    bool write_to_file(const std::filesystem::path &path) const;

    bool operator==(const CarbonIntensity &) const = default;
};

}