//
// Created by Claudio Cambra on 25/12/24. Merry Christmas!
//

#include "electricitymapscarbonintensityinterface.h"

#include <cpr/api.h>
#include <spdlog/spdlog.h>

#include <substation/geolocation/geolocation.h>

namespace
{
    
std::time_t parseISO8601(const std::string &iso8601)
{
    std::tm tm = {};
    std::istringstream ss(iso8601);

    // Parse date and time part
    ss >> std::get_time(&tm, "%Y-%m-%dT%H:%M:%S");

    if (ss.fail()) {
        const auto message = std::format("Failed to parse date: {}", iso8601);
        spdlog::error(message);
        throw std::runtime_error(message);
    }

    // Convert to time_t assuming UTC
#ifdef _WIN32
    return _mkgmtime(&tm); // Windows doesn't have timegm
#else
    return ::timegm(&tm); // Use timegm on Unix-based systems
#endif
}

}

namespace Substation::CarbonModelling
{

ElectricityMapCarbonIntensityInterface::ElectricityMapCarbonIntensityInterface(const std::string &api_key)
    : m_api_key(api_key)
{
}

std::future<std::optional<CarbonIntensity>>
ElectricityMapCarbonIntensityInterface::get_carbon_intensity(const Substation::Geolocation::Geolocation &location) const
{
    return std::async(std::launch::async, [this, &location] noexcept -> std::optional<CarbonIntensity> {
        const auto location_query = std::format("lat={}&lon={}", location.latitude, location.longitude);
        const auto response =
            cpr::Get(cpr::Url{"https://api.electricitymap.org/v3/carbon-intensity/latest?" + location_query}, cpr::Header{{"auth-token", m_api_key}});
        if (response.error.code != cpr::ErrorCode::OK) {
            spdlog::error("Failed to get carbon intensity, received response code: {}", response.status_code);
            return std::nullopt;
        }

        try {
            const auto json = nlohmann::json::parse(response.text);
            return CarbonIntensity{.zone = json.at("zone").get<std::string>(),
                                   .intensity = json.at("carbonIntensity").get<int>(),
                                   .datetime = parseISO8601(json.at("datetime").get<std::string>()),
                                   .updated_at = parseISO8601(json.at("updatedAt").get<std::string>()),
                                   .emission_factor_type = json.at("emissionFactorType").get<std::string>(),
                                   .is_estimated = json.at("isEstimated").get<bool>(),
                                   .estimation_method = json.at("estimationMethod").get<std::string>()};
        } catch (nlohmann::json::exception &e) {
            spdlog::error("Failed to parse carbon intensity response: {}", e.what());
            return std::nullopt;
        } catch (...) {
            return std::nullopt;
        }
    });
}

}