//
// Created by Claudio Cambra on 19/2/25.
//

#include "filebasedcarbonintensitystorage.h"

#include <algorithm>
#include <spdlog/spdlog.h>

#include "carbonintensity.h"

namespace
{

std::string carbon_intensity_filename(const std::time_t datetime)
{
    return std::format("ci_{}.json", datetime);
}

}

namespace Substation::CarbonModelling
{

FileBasedCarbonIntensityStorage::FileBasedCarbonIntensityStorage(const std::filesystem::path &store_folder)
    : m_store_folder_path(store_folder)
{
}

std::filesystem::path FileBasedCarbonIntensityStorage::default_store_folder()
{
    if (const auto path = std::getenv(STORE_ENV_VAR_NAME)) {
        return path;
    }
    return std::filesystem::temp_directory_path().append("carbon_intensities");
}

std::optional<FileBasedCarbonIntensityStorage::StorageError> FileBasedCarbonIntensityStorage::store(const CarbonIntensity &intensity)
{
    auto intensity_file_path = m_store_folder_path;
    intensity_file_path.append(carbon_intensity_filename(intensity.datetime));
    if (intensity.write_to_file(intensity_file_path)) {
        spdlog::info("Wrote carbon intensity to file: {}", intensity_file_path.string());
        notify_store_callbacks(intensity);
        return std::nullopt;
    } else {
        const auto message = std::format("Failed to write carbon intensity to file: {}", intensity_file_path.string());
        spdlog::error(message);
        return StorageError{.type = StorageErrorType::storage_inaccessible, .message = message};
    }
}

std::expected<std::vector<CarbonIntensity>, FileBasedCarbonIntensityStorage::StorageError> FileBasedCarbonIntensityStorage::data() const
{
    if (m_store_folder_path.empty()) {
        return std::unexpected(StorageError{.type = StorageErrorType::storage_inaccessible, .message = "Store folder path is empty."});
    } else if (!std::filesystem::exists(m_store_folder_path) && !std::filesystem::create_directories(m_store_folder_path)) {
        return std::unexpected(StorageError{.type = StorageErrorType::storage_inaccessible, .message = "Store folder path could not be created."});
    }
    return CarbonIntensity::from_files(m_store_folder_path);
}

std::expected<CarbonIntensity, FileBasedCarbonIntensityStorage::StorageError> FileBasedCarbonIntensityStorage::retrieve(const std::time_t datetime) const
{
    if (m_store_folder_path.empty()) {
        return std::unexpected(StorageError{.type = StorageErrorType::storage_inaccessible, .message = "Store folder path is empty."});
    } else if (!std::filesystem::exists(m_store_folder_path)) {
        return std::unexpected(StorageError{.type = StorageErrorType::storage_inaccessible, .message = "Store folder path does not exist."});
    }
    const auto intensity_file_path = m_store_folder_path / carbon_intensity_filename(datetime);
    if (!std::filesystem::exists(intensity_file_path)) {
        return std::unexpected(StorageError{.type = StorageErrorType::intensity_invalid, .message = "Intensity file does not exist."});
    }

    const auto intensity_opt = CarbonIntensity::from_file(intensity_file_path);
    if (!intensity_opt.has_value()) {
        return std::unexpected(StorageError{.type = StorageErrorType::intensity_invalid, .message = "Intensity file is invalid."});
    }

    return *intensity_opt;
}

std::expected<CarbonIntensity, FileBasedCarbonIntensityStorage::StorageError> FileBasedCarbonIntensityStorage::earliest_intensity() const
{
    const auto intensities = data();
    if (!intensities.has_value()) {
        return std::unexpected(intensities.error());
    } else if (intensities->empty()) {
        return std::unexpected(StorageError{.type = StorageErrorType::intensity_invalid, .message = "No intensities available."});
    }
    return *std::ranges::min_element(*intensities, std::less{}, &CarbonIntensity::datetime);
}

std::expected<CarbonIntensity, FileBasedCarbonIntensityStorage::StorageError> FileBasedCarbonIntensityStorage::latest_intensity() const
{
    const auto intensities = data();
    if (!intensities.has_value()) {
        return std::unexpected(intensities.error());
    } else if (intensities->empty()) {
        return std::unexpected(StorageError{.type = StorageErrorType::intensity_invalid, .message = "No intensities available."});
    }
    return *std::ranges::max_element(*intensities, std::less{}, &CarbonIntensity::datetime);
}

}