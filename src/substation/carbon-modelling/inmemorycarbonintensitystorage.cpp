//
// Created by Claudio Cambra on 24/2/25.
//

#include "inmemorycarbonintensitystorage.h"

#include <spdlog/spdlog.h>

namespace Substation::CarbonModelling
{

std::optional<InMemoryCarbonIntensityStorage::StorageError> InMemoryCarbonIntensityStorage::store(const CarbonIntensity &intensity)
{
    const auto [it, inserted] = m_store.try_emplace(intensity.datetime, intensity);
    if (!inserted) {
        spdlog::error("Failed to store carbon intensity with datetime: {}", intensity.datetime);
        return StorageError{.type = StorageErrorType::storage_inaccessible, .message = "Failed to store carbon intensity."};
    }
    notify_store_callbacks(intensity);
    return std::nullopt;
}

std::expected<std::vector<CarbonIntensity>, InMemoryCarbonIntensityStorage::StorageError> InMemoryCarbonIntensityStorage::data() const
{
    std::vector<CarbonIntensity> intensities;
    intensities.reserve(m_store.size());
    for (const auto &[_, intensity] : m_store) {
        intensities.push_back(intensity);
    }
    return intensities;
}

std::expected<CarbonIntensity, InMemoryCarbonIntensityStorage::StorageError> InMemoryCarbonIntensityStorage::retrieve(std::time_t datetime) const
{
    if (const auto it = m_store.find(datetime); it != m_store.end()) {
        return it->second;
    }
    return std::unexpected(StorageError{.type = StorageErrorType::intensity_invalid, .message = "Carbon intensity not found."});
}

std::expected<CarbonIntensity, InMemoryCarbonIntensityStorage::StorageError> InMemoryCarbonIntensityStorage::earliest_intensity() const
{
    if (m_store.empty()) {
        return std::unexpected(StorageError{.type = StorageErrorType::intensity_invalid, .message = "No carbon intensities stored."});
    }
    return m_store.begin()->second;
}

std::expected<CarbonIntensity, InMemoryCarbonIntensityStorage::StorageError> InMemoryCarbonIntensityStorage::latest_intensity() const
{
    if (m_store.empty()) {
        return std::unexpected(StorageError{.type = StorageErrorType::intensity_invalid, .message = "No carbon intensities stored."});
    }
    return m_store.rbegin()->second;
}

}