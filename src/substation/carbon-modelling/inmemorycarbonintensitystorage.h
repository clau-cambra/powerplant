//
// Created by Claudio Cambra on 24/2/25.
//

#pragma once

#include <map>

#include "abstractcarbonintensitystorage.h"
#include "carbonintensity.h"

namespace Substation::CarbonModelling
{

class InMemoryCarbonIntensityStorage : public AbstractCarbonIntensityStorage
{
public:
    explicit InMemoryCarbonIntensityStorage() = default;
    ~InMemoryCarbonIntensityStorage() override = default;

    std::optional<StorageError> store(const CarbonIntensity &intensity) override;
    std::expected<std::vector<CarbonIntensity>, StorageError> data() const override;
    std::expected<CarbonIntensity, StorageError> retrieve(std::time_t datetime) const override;
    std::expected<CarbonIntensity, StorageError> earliest_intensity() const override;
    std::expected<CarbonIntensity, StorageError> latest_intensity() const override;

private:
    std::map<std::time_t, CarbonIntensity> m_store;
};

}