//
// Created by Claudio Cambra on 19/2/25.
//

#pragma once

#include <ctime>
#include <expected>
#include <functional>
#include <optional>
#include <vector>

namespace Substation::CarbonModelling
{

struct CarbonIntensity;

class AbstractCarbonIntensityStorage
{
public:
    enum class StorageErrorType {
        storage_inaccessible,
        intensity_invalid
    };

    struct StorageError {
        StorageErrorType type;
        std::string message;
    };

    virtual ~AbstractCarbonIntensityStorage() = default;
    virtual std::optional<StorageError> store(const CarbonIntensity &intensity) = 0;
    virtual std::expected<std::vector<CarbonIntensity>, StorageError> data() const = 0;
    virtual std::expected<CarbonIntensity, StorageError> retrieve(std::time_t datetime) const = 0;
    virtual std::expected<CarbonIntensity, StorageError> earliest_intensity() const = 0;
    virtual std::expected<CarbonIntensity, StorageError> latest_intensity() const = 0;

    void register_store_callback(const std::function<void(const CarbonIntensity &)> &callback)
    {
        m_store_callbacks.push_back(callback);
    };

protected:
    void notify_store_callbacks(const CarbonIntensity &intensity) const
    {
        for (const auto &callback : m_store_callbacks) {
            callback(intensity);
        }
    }

private:
    std::vector<std::function<void(const CarbonIntensity &)>> m_store_callbacks;
};

}