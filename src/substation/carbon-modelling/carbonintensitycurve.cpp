//
// Created by Claudio Cambra on 4/2/25.
//

#include "carbonintensitycurve.h"

#include <algorithm>
#include <chrono>
#include <ctime>
#include <format>
#include <iomanip>
#include <iostream>
#include <iterator>
#include <limits>
#include <sstream>
#include <string>
#include <vector>

#include <spdlog/spdlog.h>

namespace Substation::CarbonModelling
{

namespace CurveMath
{

struct CurveSection {
    double area = -1.0;
    int min_intensity_index = -1;
    int max_intensity_index = -1;
};

inline double trapezoid_area(const double a, const double b, const double h)
{
    return (a + b) * h / 2.0;
}

inline CurveSection area_under_curve_period(const std::span<const CarbonIntensity> &curve_span)
{
    assert(!curve_span.empty());
    double area = 0.0;
    auto min_intensity_index = 0;
    auto max_intensity_index = 0;
    for (auto i = 0; i < curve_span.size() - 1; ++i) {
        const auto curr_index = i;
        const auto next_index = i + 1;
        const auto &curr = curve_span[curr_index];
        const auto &next = curve_span[next_index];
        const auto trapezoid_h = static_cast<double>(next.datetime - curr.datetime);
        area += trapezoid_area(curr.intensity, next.intensity, trapezoid_h);

        const auto &lower_intensity_index = curr.intensity < next.intensity ? curr_index : next_index;
        const auto &upper_intensity_index = curr.intensity > next.intensity ? curr_index : next_index;
        if (curve_span[lower_intensity_index].intensity < curve_span[min_intensity_index].intensity) {
            min_intensity_index = lower_intensity_index;
        }
        if (curve_span[upper_intensity_index].intensity > curve_span[max_intensity_index].intensity) {
            max_intensity_index = upper_intensity_index;
        }
    }
    return {.area = area, .min_intensity_index = min_intensity_index, .max_intensity_index = max_intensity_index};
}

}

namespace IntensityMath
{

constexpr std::time_t TIME_T_24_HRS = std::chrono::duration_cast<std::chrono::seconds>(std::chrono::hours(24)).count();

inline bool time_at_least_24hrs_ago(const std::time_t time, const std::time_t from)
{
    using namespace std::chrono;
    return system_clock::from_time_t(from) - system_clock::from_time_t(time) >= hours(24);
}

CarbonIntensity ci_to_24hr_prediction(const CarbonIntensity &ci)
{
    auto new_ci = ci;
    new_ci.datetime += TIME_T_24_HRS;
    new_ci.is_estimated = true;
    new_ci.estimation_method = "24h-ago";
    new_ci.is_forecast = true;
    new_ci.forecast_method = "24h-ago";
    new_ci.is_synthetic = true;
    return new_ci;
}

}

CarbonIntensityCurve::CarbonIntensityCurve(const std::vector<CarbonIntensity> &intensities, const std::shared_ptr<AbstractCarbonIntensityStorage> &storage)
    : m_intensities{intensities}
    , m_storage{storage}
{
    if (m_storage) {
        m_storage->register_store_callback([this](const CarbonIntensity &intensity) {
            add_intensity(intensity);
        });
    }
}

std::unique_lock<std::mutex> CarbonIntensityCurve::setup_lock(const bool use_mutex) const
{
    std::unique_lock<std::mutex> lock;
    if (use_mutex) {
        lock = std::unique_lock(m_intensities_mutex);
    }
    return std::move(lock);
}

void CarbonIntensityCurve::add_intensity(const CarbonIntensity &intensity, const bool use_mutex)
{
    const auto lock = setup_lock(use_mutex);

    if (m_intensities.empty()) {
        m_intensities.push_back(intensity);
        return;
    }

    // Find where to insert the intensity
    const auto it = std::ranges::lower_bound(m_intensities, intensity, [](const CarbonIntensity &a, const CarbonIntensity &b) {
        return a.datetime < b.datetime;
    });
    // Insert before larger intensity
    m_intensities.insert(it, intensity);

    // If this is a real (not synthetic) carbon intensity...
    if (it == m_intensities.begin() || it->is_synthetic) {
        return;
    }
    // ...remove synthetic interpolated intensities between this and previous synthetic intensity
    auto prev = std::prev(it);
    while (prev != m_intensities.begin() && prev->is_synthetic) {
        const auto next_prev = std::prev(prev);
        m_intensities.erase(prev, it);
        prev = next_prev;
    }
}

size_t CarbonIntensityCurve::intensity_count(const IntensityTypeFlags types) const
{
    const auto lock = setup_lock(false);
    if (types == all) {
        return m_intensities.size();
    }
    return std::ranges::count_if(m_intensities, [types](const CarbonIntensity &ci) {
        return (types & forecast && ci.is_forecast) || (types & synthetic && ci.is_synthetic) || (!ci.is_forecast && !ci.is_synthetic);
    });
}

std::optional<CarbonIntensity> CarbonIntensityCurve::intensity_at(const std::time_t time, const PredictionMethodFlags prediction_flags, const bool use_mutex)
{
    const auto lock = setup_lock(use_mutex);
    
    // If there is an exact match, return it (though this may be rare).
    // Otherwise find the closes two intensities and interpolate.
    const auto it = std::ranges::lower_bound(m_intensities, time, std::less{}, [](const CarbonIntensity &ci) {
        return ci.datetime;
    });

    if (it->datetime == time) {
        return *it;
    } else if (it == m_intensities.begin() || it == m_intensities.end()) {
        return predicted_intensity(time, prediction_flags);
    }
    const auto prev = std::prev(it);
    const auto interpolated_intensity = CarbonIntensity::interpolate(*prev, *it, time);
    add_intensity(interpolated_intensity, false);
    return interpolated_intensity;
}

inline bool CarbonIntensityCurve::have_data_24_hours_prior(const std::time_t time) const
{
    return !m_intensities.empty() && IntensityMath::time_at_least_24hrs_ago(m_intensities.front().datetime, time);
}

std::optional<CarbonIntensity> CarbonIntensityCurve::predicted_intensity(const std::time_t time, const PredictionMethodFlags prediction_flags)
{
    if (m_intensities.empty()) {
        return std::nullopt;
    }

    if ((prediction_flags & PredictionMethodFlags::use_past_24h || prediction_flags & PredictionMethodFlags::loose_use_past_24h)
        && have_data_24_hours_prior(time)) {
        // Find an intensity that was 24 hours ago, interpolated if needed
        const auto predicted_it_opt = intensity_at(time - IntensityMath::TIME_T_24_HRS, none, false); // Do not use prediction flags here
        assert(predicted_it_opt.has_value());
        return IntensityMath::ci_to_24hr_prediction(*predicted_it_opt);
    }
    return std::nullopt;
}

std::optional<std::vector<CarbonIntensity>>
CarbonIntensityCurve::predict_intensities_for_search(const std::time_t search_start, const std::time_t search_end, const PredictionMethodFlags prediction_flags)
{
    if (m_intensities.empty()) {
        return std::nullopt;
    }

    auto predicted_intensities = m_intensities;
    if (const auto &predicted_back = predicted_intensities.back();
        prediction_flags & PredictionMethodFlags::use_past_24h && search_end > predicted_back.datetime && have_data_24_hours_prior(predicted_back.datetime)) {
        // Fill in data points from the last known intensity up to the search end based on previous 24 hour data.
        const auto predicted_back_24hrs_ago = predicted_back.datetime - IntensityMath::TIME_T_24_HRS;
        const auto search_end_24hrs_ago = search_end - IntensityMath::TIME_T_24_HRS;
        const std::span<const CarbonIntensity> fixed_range(predicted_intensities.data(), predicted_intensities.size());
        auto it = std::ranges::upper_bound(fixed_range, predicted_back_24hrs_ago, std::less{}, [](const CarbonIntensity &ci) {
            return ci.datetime;
        });
        while (it != fixed_range.end() && it->datetime <= search_end_24hrs_ago) {
            const auto predicted_ci_opt = intensity_at(it->datetime + IntensityMath::TIME_T_24_HRS, prediction_flags, false);
            assert(predicted_ci_opt.has_value());
            predicted_intensities.push_back(*predicted_ci_opt);
            ++it;
        }
    } else if (prediction_flags & PredictionMethodFlags::loose_use_past_24h && search_end > predicted_back.datetime
               && have_data_24_hours_prior(search_end + IntensityMath::TIME_T_24_HRS)) {
        // Fill in as much data as possible (up to m_intensities earliest value) based on previous 24 hour data.
        // Will leave a gap between the last value in m_intensities and the first new predicted value.
        const auto search_end_24hrs_ago = search_end - IntensityMath::TIME_T_24_HRS;
        const std::span<const CarbonIntensity> fixed_range(predicted_intensities.data(), predicted_intensities.size());
        const auto bound_it = std::ranges::upper_bound(fixed_range, search_end_24hrs_ago, std::less{}, [](const CarbonIntensity &ci) {
            return ci.datetime;
        });
        auto it = std::make_reverse_iterator(bound_it);
        auto insertion_it = predicted_intensities.end();
        // Also make sure to prevent duplicates when we have a full 24 hour range (hence the datetime check)
        while (it != fixed_range.rend() && it->datetime + IntensityMath::TIME_T_24_HRS > fixed_range.back().datetime) {
            const auto predicted_ci_opt = intensity_at(it->datetime + IntensityMath::TIME_T_24_HRS, prediction_flags, false);
            assert(predicted_ci_opt.has_value());
            insertion_it = predicted_intensities.insert(insertion_it, *predicted_ci_opt);
            ++it;
        }
    }

    if (predicted_intensities.front().datetime <= search_start && predicted_intensities.back().datetime >= search_end) {
        return predicted_intensities;
    }
    return std::nullopt;
}

std::optional<std::pair<CarbonIntensityCurve::error, std::string>>
CarbonIntensityCurve::validate_search_parameters(const std::time_t search_start,
                                                 const std::time_t search_end,
                                                 const std::time_t duration,
                                                 const std::vector<CarbonIntensity> &search_vector)
{
    using enum error;
    if (search_vector.size() <= 2) {
        return std::make_pair(no_data, "Not enough data to find lowest intensity period.");
    } else if (search_start < search_vector.front().datetime) {
        return std::make_pair(request_too_far_in_past, "Search start time is too far in the past.");
    } else if (search_end > search_vector.back().datetime) {
        return std::make_pair(request_too_far_in_future, "Search end time is too far in the future.");
    } else if (search_start > search_end) {
        return std::make_pair(request_invalid_times, "Search start must precede search_end");
    } else if (duration > search_end - search_start) {
        return std::make_pair(request_invalid_duration, "Duration is longer than the search period.");
    }
    return std::nullopt;
}

std::expected<CarbonIntensityCurve::Period, CarbonIntensityCurve::error>
CarbonIntensityCurve::lowest_intensity_period(const std::time_t search_start,
                                              const std::time_t search_end,
                                              const std::time_t duration,
                                              const PredictionMethodFlags prediction_flags)
{
    const auto lock = setup_lock(true);

    const auto *search_intensities = &m_intensities;
    std::optional<std::vector<CarbonIntensity>> synthetic_intensities;

    auto encountered_error = validate_search_parameters(search_start, search_end, duration, *search_intensities);
    const auto perform_prediction = prediction_flags != PredictionMethodFlags::none && encountered_error.has_value()
        && (encountered_error->first == error::request_too_far_in_past || encountered_error->first == error::request_too_far_in_future);

    if (perform_prediction) {
        spdlog::debug("Generating synthetic intensities to fill in for missing data.");
        synthetic_intensities = predict_intensities_for_search(search_start, search_end, prediction_flags);
        if (synthetic_intensities.has_value()) {
            search_intensities = &(*synthetic_intensities);
            encountered_error = validate_search_parameters(search_start, search_end, duration, *search_intensities);
        }
    }

    if (encountered_error.has_value()) {
        spdlog::debug("Could not provide lowest intensity period: {}", encountered_error->second);
        return std::unexpected(encountered_error->first);
    }

    // Find the first intensity that is at the same time, or after, search_start
    auto start_it = std::ranges::lower_bound(*search_intensities, search_start, std::less{}, [](const CarbonIntensity &ci) {
        return ci.datetime;
    });
    // Find the first intensity that is after the search_start plus the duration
    auto end_it = std::ranges::lower_bound(*search_intensities, search_start + duration, std::less{}, [](const CarbonIntensity &ci) {
        return ci.datetime;
    });
    if (end_it == start_it) {
        ++end_it;
    }
    const auto final_it = std::ranges::lower_bound(*search_intensities, search_end, std::less{}, [](const CarbonIntensity &ci) {
        return ci.datetime;
    });

    // We are using a window since we have a fixed duration to deal with.
    // With this window, we search for the lowest intensity period.
    // We are essentially looking for the area under the line of the curve with the lowest value.
    // If we reach the curve's or the intensity with a datetime equal to search_end, we stop.
    auto lowest_start_it = start_it;
    auto lowest_end_it = end_it;
    CurveMath::CurveSection lowest_area_period{.area = std::numeric_limits<double>::max()};

    // Slide the window through the time period
    while (end_it != search_intensities->end() && end_it <= final_it) {
        // Note to self: std::span expects an end iterator in constructor as one beyond the final value
        // TODO: Cache area calculations
        if (const auto area_section = CurveMath::area_under_curve_period({start_it, std::next(end_it)}); area_section.area < lowest_area_period.area) {
            lowest_area_period = area_section;
            lowest_start_it = start_it;
            lowest_end_it = end_it;
        }
        ++start_it;
        ++end_it;
    }

    return Period{.intensities = std::vector<CarbonIntensity>(lowest_start_it, std::next(lowest_end_it)),
                  .min_intensity_index = lowest_area_period.min_intensity_index,
                  .max_intensity_index = lowest_area_period.max_intensity_index};
}

void CarbonIntensityCurve::print_ascii_graph() const
{
    const auto lock = setup_lock(true);
    if (m_intensities.empty()) {
        spdlog::error("No carbon intensity data available.");
        return;
    }

    // Determine the min and max intensity.
    auto min_intensity = std::numeric_limits<double>::max();
    auto max_intensity = std::numeric_limits<double>::lowest();
    for (const auto &ci : m_intensities) {
        min_intensity = std::min(min_intensity, static_cast<double>(ci.intensity));
        max_intensity = std::max(max_intensity, static_cast<double>(ci.intensity));
    }
    const auto range = max_intensity - min_intensity;

    // Adapt the grid height:
    // For small ranges, use a finer resolution (at least 10 rows),
    // or cap the height for large ranges.
    constexpr auto MIN_HEIGHT = 10;
    constexpr auto MAX_HEIGHT = 20;
    const auto height = (range < static_cast<double>(MIN_HEIGHT)) ? MIN_HEIGHT : std::min(MAX_HEIGHT, static_cast<int>(std::ceil(range)));

    // Limit the graph width to a maximum number of columns.
    const auto orig_width = m_intensities.size();
    constexpr size_t MAX_GRAPH_WIDTH = 80;
    const auto effective_width = std::min(orig_width, MAX_GRAPH_WIDTH);

    // Create a grid (vector of strings) with effective_width columns.
    std::vector<std::string> grid(height, std::string(effective_width, ' '));

    // Group intensity points into buckets if necessary.
    for (auto col = 0; col < effective_width; ++col) {
        // Determine the bucket boundaries.
        const auto bucket_start = col * orig_width / effective_width;
        const auto bucket_end = (col + 1) * orig_width / effective_width;
        double bucket_sum = 0.0;
        const auto count = bucket_end - bucket_start;
        for (auto i = bucket_start; i < bucket_end; ++i) {
            bucket_sum += m_intensities[i].intensity;
        }
        const auto bucket_intensity = (count > 0) ? (bucket_sum / count) : 0.0;

        int row;
        if (range == 0) {
            row = height / 2;
        } else {
            double normalized = (bucket_intensity - min_intensity) / range;
            // Reverse mapping so higher intensities are at the top.
            row = height - 1 - static_cast<int>(std::round(normalized * (height - 1)));
        }
        if (row >= 0 && row < height && col < grid[row].size()) {
            grid[row][col] = '*';
        }
    }

    // Decide if we need to scale the labels.
    const auto scale_labels = max_intensity >= 10000.0;
    const auto scale_factor = scale_labels ? 1000.0 : 1.0;
    std::string label_suffix = scale_labels ? "k" : "";

    // Print the graph with axis labels.
    std::cout << "Carbon Intensity ASCII Graph:\n\n";

    // Print vertical axis labels (left) with the grid.
    // Each row is labeled with the calibrated intensity corresponding to that row.
    for (auto r = 0; r < height; ++r) {
        const auto intensity_value = height <= 1 ? min_intensity : min_intensity + ((height - 1 - r) * range) / (height - 1);
        std::cout << std::format("{:>8.2f}{} | {}\n", (intensity_value / scale_factor), label_suffix, grid[r]);
    }

    // Print the x-axis line.
    std::cout << std::string(8, ' ') << "+" << std::string(effective_width, '-') << "\n";

    // Print tick marks on the x-axis.
    std::string tick_line(effective_width, ' ');
    // Place ticks at approximately 0%, 25%, 50%, 75%, and 100% of the effective_width.
    std::array<size_t, 5> tick_positions = {0,
                                            static_cast<size_t>(effective_width * 0.25),
                                            static_cast<size_t>(effective_width * 0.5),
                                            static_cast<size_t>(effective_width * 0.75),
                                            effective_width - 1};
    for (const auto pos : tick_positions) {
        if (pos < effective_width) {
            tick_line[pos] = '|';
        }
    }
    std::cout << std::string(8, ' ') << tick_line << "\n";

    // Print time labels underneath the tick marks.
    // For each tick position, convert the corresponding original index time to a HH:MM label.
    std::string time_label_line(effective_width, ' ');
    for (const auto pos : tick_positions) {
        // Map tick position back to an index in m_intensities.
        const auto orig_index = pos * orig_width / effective_width;
        const auto t = m_intensities[orig_index].datetime;
        // Format time as HH:MM.
        const auto time_point = std::chrono::system_clock::from_time_t(t);
        const auto time_t = std::chrono::system_clock::to_time_t(time_point);
        std::tm tm;
        ::localtime_r(&time_t, &tm);
        std::ostringstream oss;
        oss << std::put_time(&tm, "%H:%M");
        std::string tlabel = oss.str();

        // Place the label starting at the tick position.
        // If the label is wider than the space available, print as many characters as possible.
        for (auto j = 0; j < tlabel.size() && (pos + j) < effective_width; ++j) {
            time_label_line[pos + j] = tlabel[j];
        }
    }
    std::cout << std::string(8, ' ') << time_label_line << "\n";
}

}