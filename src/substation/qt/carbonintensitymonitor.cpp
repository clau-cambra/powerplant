//
// Created by Claudio Cambra on 25/2/25.
//

#include "carbonintensitymonitor.h"

#include "carbonintensity.h"
#include "geolocation.h"
#include "substation/carbon-modelling/carbonintensitymonitor.h"

namespace Substation::Qt
{

CarbonIntensityMonitor::CarbonIntensityMonitor(const std::shared_ptr<Substation::Geolocation::AbstractGeolocationInterface> &glInterface,
                                               const std::shared_ptr<Substation::CarbonModelling::AbstractCarbonIntensityInterface> &ciInterface,
                                               const std::shared_ptr<Substation::CarbonModelling::AbstractCarbonIntensityStorage> &ciStorage,
                                               const std::chrono::seconds interval,
                                               QObject *const parent)
    : QObject{parent}
    , m_ssMonitor(Substation::CarbonModelling::CarbonIntensityMonitor::create(glInterface, ciInterface, ciStorage, {}, interval))
{
    m_ssMonitor->add_intensity_fetch_started_callback([this] {
        m_fetchingCarbonIntensity = true;
        emit carbonIntensityFetchStarted();
        emit fetchingChanged(fetching());
    });
    m_ssMonitor->add_intensity_fetched_callback([this, ciStorage](const Substation::CarbonModelling::CarbonIntensity &intensity) {
        m_fetchingCarbonIntensity = false;
        if (ciStorage) {
            ciStorage->store(intensity);
        }
        emit carbonIntensityFetched(CarbonIntensity(intensity));
        emit fetchingChanged(fetching());
    });
    m_ssMonitor->add_geolocation_fetch_started_callback([this] {
        m_fetchingGeolocation = true;
        emit geolocationFetchStarted();
        emit fetchingChanged(fetching());
    });
    m_ssMonitor->add_geolocation_fetched_callback([this](const Substation::Geolocation::Geolocation &geolocation) {
        const auto qGeolocation = Geolocation(geolocation);
        m_latestGeolocation = qGeolocation;
        m_fetchingGeolocation = false;
        emit geolocationFetched(qGeolocation);
        emit fetchingChanged(fetching());
    });
}

bool CarbonIntensityMonitor::running() const
{
    return m_ssMonitor->running();
}

void CarbonIntensityMonitor::setRunning(const bool running)
{
    if (running) {
        start();
    } else {
        stop();
    }
}

bool CarbonIntensityMonitor::fetching() const
{
    return m_fetchingCarbonIntensity || m_fetchingGeolocation;
}

void CarbonIntensityMonitor::start()
{
    m_ssMonitor->start();
    emit runningChanged(running());
}

void CarbonIntensityMonitor::stop()
{
    m_ssMonitor->stop();
    emit runningChanged(running());
}

void CarbonIntensityMonitor::fetch()
{
    m_ssMonitor->fetch();
}

Geolocation CarbonIntensityMonitor::geolocation() const
{
    return m_latestGeolocation.value();
}

bool CarbonIntensityMonitor::geolocationAvailable() const
{
    return m_latestGeolocation.has_value();
}

}