//
// Created by Claudio Cambra on 24/2/25.
//

#ifndef QT_CARBONINTENSITYMODEL_H
#define QT_CARBONINTENSITYMODEL_H

#include <QAbstractTableModel>

namespace Substation::CarbonModelling
{
class AbstractCarbonIntensityStorage;
class CarbonIntensityCurve;
}

namespace Substation::Qt
{

class CarbonIntensityModel : public QAbstractTableModel
{
    Q_OBJECT

public:
    explicit CarbonIntensityModel(const std::shared_ptr<Substation::CarbonModelling::AbstractCarbonIntensityStorage> &storage = {},
                                  size_t curveIngestionLimit = 0,
                                  QObject *const parent = nullptr);

    enum Roles {
        IntensityRole = ::Qt::UserRole + 1,
        ZoneRole,
        DateTimeRole,
        UpdatedAtRole,
        EmissionFactorTypeRole,
        IsEstimatedRole,
        EstimationMethodRole,
        IsForecastRole,
        ForecastMethodRole,
        IsSyntheticRole
    };
    Q_ENUM(Roles)

    enum Columns {
        DefaultColumn = 0,
        DateTimeColumn,
        IntensityColumn,
        ColumnCount
    };
    Q_ENUM(Columns)

    int rowCount(const QModelIndex &parent = QModelIndex()) const override;
    int columnCount(const QModelIndex &parent = QModelIndex()) const override;
    QVariant data(const QModelIndex &index, int role = ::Qt::DisplayRole) const override;

    std::shared_ptr<Substation::CarbonModelling::CarbonIntensityCurve> curve() const;

private:
    std::shared_ptr<Substation::CarbonModelling::AbstractCarbonIntensityStorage> m_storage;
    std::shared_ptr<Substation::CarbonModelling::CarbonIntensityCurve> m_curve;
};

}

Q_DECLARE_METATYPE(Substation::Qt::CarbonIntensityModel)

#endif // QT_CARBONINTENSITYMODEL_H
