//
// Created by Claudio Cambra on 24/2/25.
//

#include "carbonintensity.h"

namespace Substation::Qt
{

CarbonIntensity::CarbonIntensity(const Substation::CarbonModelling::CarbonIntensity &ssCi)
: m_ssCi{ssCi}
{
}

QString CarbonIntensity::zone() const
{
    return QString::fromStdString(m_ssCi.zone);
}

int CarbonIntensity::intensity() const
{
    return m_ssCi.intensity;
}

QDateTime CarbonIntensity::dateTime() const
{
    return QDateTime::fromSecsSinceEpoch(m_ssCi.datetime);
}

QDateTime CarbonIntensity::updatedAt() const
{
    return QDateTime::fromSecsSinceEpoch(m_ssCi.updated_at);
}

QString CarbonIntensity::emissionFactorType() const
{
    return QString::fromStdString(m_ssCi.emission_factor_type);
}

bool CarbonIntensity::isEstimated() const
{
    return m_ssCi.is_estimated;
}

QString CarbonIntensity::estimationMethod() const
{
    return QString::fromStdString(m_ssCi.estimation_method);
}

bool CarbonIntensity::isForecast() const
{
    return m_ssCi.is_forecast;
}

QString CarbonIntensity::forecastMethod() const
{
    return QString::fromStdString(m_ssCi.forecast_method);
}

bool CarbonIntensity::isSynthetic() const
{
    return m_ssCi.is_synthetic;
}

Substation::CarbonModelling::CarbonIntensity CarbonIntensity::substationCarbonIntensity() const
{
    return m_ssCi;
}

bool CarbonIntensity::operator==(const CarbonIntensity &other) const
{
    return m_ssCi == other.m_ssCi;
}

}