//
// Created by Claudio Cambra on 24/2/25.
//

#include "geolocationprovider.h"

#include <QtConcurrent/QtConcurrent>

namespace Substation::Qt
{

GeolocationProvider::GeolocationProvider(const std::shared_ptr<Substation::Geolocation::AbstractGeolocationInterface> &geolocationInterface,
                                         const std::chrono::minutes checkInterval,
                                         QObject *const parent)
    : QObject{parent}
    , m_checkInterval{checkInterval}
    , m_glIntf{geolocationInterface}
{
    checkGeolocation();
    m_checkTimer.setInterval(checkInterval);
    m_checkTimer.callOnTimeout(this, &GeolocationProvider::checkGeolocation);
}

bool GeolocationProvider::geolocationAvailable() const
{
    return m_geolocation.has_value();
}

Geolocation GeolocationProvider::geolocation() const
{
    return m_geolocation.value();
}

bool GeolocationProvider::retrieving() const
{
    return m_retrieving;
}

void GeolocationProvider::setRetrieving(const bool retrieving)
{
    if (retrieving == m_retrieving) {
        return;
    }
    m_retrieving = retrieving;
    emit retrievingChanged(retrieving);
}

void GeolocationProvider::checkGeolocation()
{
    if (retrieving()) {
        qWarning() << "Ongoing retrieval, won't check geolocation yet.";
        return;
    }

    Q_ASSERT(m_glIntf);
    qDebug() << "Fetching current geolocation.";
    setRetrieving(true);
    const auto future = QtConcurrent::run([this] {
                            return m_glIntf->get_geolocation().get();
                        })
                            .then([this](const std::optional<Substation::Geolocation::Geolocation> &ssGlOpt) {
                                if (ssGlOpt.has_value()) {
                                    const auto &ssGl = *ssGlOpt;
                                    if (!m_geolocation.has_value() || ssGl != m_geolocation->substationGeolocation()) {
                                        m_geolocation = Substation::Qt::Geolocation(*ssGlOpt);
                                        emit geolocationChanged(*m_geolocation);
                                    }
                                    emit geolocationRetrieved(*m_geolocation);
                                    qInfo() << "Retrieved current geolocation!" << m_geolocation->name() << m_geolocation->country()
                                            << m_geolocation->latitude() << m_geolocation->longitude();
                                } else {
                                    qWarning() << "Could not retrieve geolocation! Will retry later.";
                                }
                            })
                            .then([this] {
                                setRetrieving(false);
                            });
    m_ongoingFutureWatcher.setFuture(future);
}
}
