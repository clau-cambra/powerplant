set(QT_PUBLIC_HEADERS
    carbonintensity.h
    carbonintensitymodel.h
    carbonintensitymonitor.h
    geolocation.h
    geolocationprovider.h
)
add_library(qt
    ${QT_PUBLIC_HEADERS}
    carbonintensity.cpp
    carbonintensitymodel.cpp
    carbonintensitymonitor.cpp
    geolocation.cpp
    geolocationprovider.cpp
)
target_include_directories(qt
    PUBLIC
    $<BUILD_INTERFACE:${CMAKE_SOURCE_DIR}/src>
    $<INSTALL_INTERFACE:include>
)
target_link_libraries(qt PUBLIC Qt6::Core common carbon-modelling)
set_target_properties(qt PROPERTIES
    PREFIX ${LIBSUBSTATION_PREFIX}
    PUBLIC_HEADER "${QT_PUBLIC_HEADERS}"
)

install(TARGETS qt
    EXPORT qt-targets
    ARCHIVE DESTINATION lib
    LIBRARY DESTINATION lib
    RUNTIME DESTINATION bin
    PUBLIC_HEADER DESTINATION include/substation/qt
)

# Export the targets for other projects
install(EXPORT qt-targets
    FILE qt-config.cmake
    NAMESPACE substation::
    DESTINATION lib/cmake/substation
)

if (BUILD_TESTS)
    add_subdirectory(tests)
endif()