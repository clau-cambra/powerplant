//
// Created by Claudio Cambra on 24/2/25.
//

#ifndef QT_GEOLOCATIONPROVIDER_H
#define QT_GEOLOCATIONPROVIDER_H

#include <QFutureWatcher>
#include <QObject>
#include <QTimer>

#include <chrono>

#include <substation/geolocation/abstractgeolocationinterface.h>
#include <substation/qt/geolocation.h>

namespace Substation::Qt
{

using namespace std::chrono_literals;

class GeolocationProvider : public QObject
{
    Q_OBJECT

    Q_PROPERTY(bool geolocationAvailable READ geolocationAvailable NOTIFY geolocationRetrieved)
    Q_PROPERTY(Geolocation geolocation READ geolocation NOTIFY geolocationChanged)
    Q_PROPERTY(bool retrieving READ retrieving NOTIFY retrievingChanged)

public:
    explicit GeolocationProvider(const std::shared_ptr<Substation::Geolocation::AbstractGeolocationInterface> &geolocationInterface,
                                 const std::chrono::minutes checkInterval = 60min,
                                 QObject *const parent = nullptr);

    bool geolocationAvailable() const;
    Geolocation geolocation() const;
    bool retrieving() const;

signals:
    void geolocationRetrieved(const Substation::Qt::Geolocation &geolocation);
    void geolocationChanged(const Substation::Qt::Geolocation &geolocation);
    void retrievingChanged(bool retrieving);

public slots:
    void checkGeolocation();

private:
    void setRetrieving(bool retrieving);

    std::chrono::minutes m_checkInterval;
    std::shared_ptr<Substation::Geolocation::AbstractGeolocationInterface> m_glIntf;
    QTimer m_checkTimer{this};
    QFutureWatcher<void> m_ongoingFutureWatcher;
    std::optional<Geolocation> m_geolocation;
    bool m_retrieving = false;
};

}

Q_DECLARE_METATYPE(Substation::Qt::GeolocationProvider)

#endif // GEOLOCATIONPROVIDER_H
