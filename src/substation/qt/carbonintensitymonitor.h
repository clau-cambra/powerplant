//
// Created by Claudio Cambra on 25/2/25.
//

#ifndef QT_CARBONINTENSITYMONITOR_H
#define QT_CARBONINTENSITYMONITOR_H

#include <QObject>

#include <substation/carbon-modelling/carbonintensitymonitor.h>

#include "geolocation.h"

namespace Substation::Qt
{

class CarbonIntensity;

class CarbonIntensityMonitor : public QObject
{
    Q_OBJECT

    Q_PROPERTY(bool running READ running WRITE setRunning NOTIFY runningChanged)
    Q_PROPERTY(Substation::Qt::Geolocation geolocation READ geolocation NOTIFY geolocationFetched)
    Q_PROPERTY(bool geolocationAvailable READ geolocationAvailable NOTIFY geolocationFetched)
    Q_PROPERTY(bool fetching READ fetching NOTIFY fetchingChanged)

public:
    static constexpr auto DefaultInterval = std::chrono::duration_cast<std::chrono::seconds>(std::chrono::minutes{15});

    explicit CarbonIntensityMonitor(const std::shared_ptr<Substation::Geolocation::AbstractGeolocationInterface> &glInterface,
                                    const std::shared_ptr<Substation::CarbonModelling::AbstractCarbonIntensityInterface> &ciInterface,
                                    const std::shared_ptr<Substation::CarbonModelling::AbstractCarbonIntensityStorage> &ciStorage = {},
                                    const std::chrono::seconds interval = DefaultInterval,
                                    QObject *const parent = nullptr);

    bool running() const;
    void setRunning(bool running);

    Geolocation geolocation() const;
    bool geolocationAvailable() const;

    bool fetching() const;

signals:
    void carbonIntensityFetchStarted();
    void carbonIntensityFetched(const Substation::Qt::CarbonIntensity &ci);
    void geolocationFetchStarted();
    void geolocationFetched(const Substation::Qt::Geolocation &gl);
    void runningChanged(bool running);
    void fetchingChanged(bool fetching);

public slots:
    void start();
    void stop();
    void fetch();

private:
    std::optional<Substation::Qt::Geolocation> m_latestGeolocation;
    bool m_fetchingGeolocation = false;
    bool m_fetchingCarbonIntensity = false;
    std::shared_ptr<CarbonModelling::CarbonIntensityMonitor> m_ssMonitor;
};

}

Q_DECLARE_METATYPE(Substation::Qt::CarbonIntensityMonitor)

#endif
