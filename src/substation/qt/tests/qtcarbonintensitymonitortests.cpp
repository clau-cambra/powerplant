//
// Created by Claudia Cambra on 26/2/25.
//

#include "substation/qt/carbonintensity.h"
#include <catch2/catch_all.hpp>

#include <substation/carbon-modelling/tests/carbonmodellingtestutils.h>
#include <substation/qt/carbonintensitymonitor.h>

using namespace Substation::CarbonModelling::TestUtils;

TEST_CASE("Carbon intensity monitor autmoatically retrieves carbon intensity", "[qt-carbon-intensity-monitor-auto]")
{
    auto carbon_intensity_acquired = false;
    Substation::Qt::CarbonIntensity carbon_intensity({});
    auto geolocation_acquired = false;
    Substation::Qt::Geolocation geolocation({});

    auto monitor = Substation::Qt::CarbonIntensityMonitor(gl_interface, ci_interface, {}, std::chrono::seconds{1});
    QObject::connect(&monitor,
                     &Substation::Qt::CarbonIntensityMonitor::carbonIntensityFetched,
                     [&carbon_intensity_acquired, &carbon_intensity](const Substation::Qt::CarbonIntensity &fetchedIntensity) {
                         carbon_intensity_acquired = true;
                         carbon_intensity = fetchedIntensity;
                     });
    QObject::connect(&monitor,
                     &Substation::Qt::CarbonIntensityMonitor::geolocationFetched,
                     [&geolocation_acquired, &geolocation](const Substation::Qt::Geolocation &fetchedGeolocation) {
                         geolocation_acquired = true;
                         geolocation = fetchedGeolocation;
                     });

    monitor.start();
    std::this_thread::sleep_for(std::chrono::seconds(2));
    monitor.stop();

    REQUIRE(carbon_intensity_acquired);
    REQUIRE(carbon_intensity.substationCarbonIntensity() == TEST_CARBON_INTENSITY);

    REQUIRE(geolocation_acquired);
    REQUIRE(geolocation.substationGeolocation() == TEST_GEOLOCATION);
}