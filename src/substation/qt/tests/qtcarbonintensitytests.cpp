//
// Created by Claudio Cambra on 27/12/24.
//

#include <catch2/catch_all.hpp>

#include <substation/qt/carbonintensity.h>

TEST_CASE("Carbon intensity Qt wrapper returns expected values", "[carbon-intensity-qt-values]")
{
    const auto carbon_intensity = Substation::CarbonModelling::CarbonIntensity{.zone = "ES",
                                                                               .intensity = 100,
                                                                               .datetime = 1640601600,
                                                                               .updated_at = 1640601600,
                                                                               .emission_factor_type = "unknown",
                                                                               .is_estimated = false,
                                                                               .estimation_method = "unknown",
                                                                               .is_forecast = false,
                                                                               .forecast_method = "unknown",
                                                                               .is_synthetic = false};
    const auto qt_carbon_intensity = Substation::Qt::CarbonIntensity{carbon_intensity};
    REQUIRE(qt_carbon_intensity.zone() == "ES");
    REQUIRE(qt_carbon_intensity.intensity() == 100);
    REQUIRE(qt_carbon_intensity.dateTime() == QDateTime::fromSecsSinceEpoch(1640601600));
    REQUIRE(qt_carbon_intensity.updatedAt() == QDateTime::fromSecsSinceEpoch(1640601600));
    REQUIRE(qt_carbon_intensity.emissionFactorType() == "unknown");
    REQUIRE(qt_carbon_intensity.isEstimated() == false);
    REQUIRE(qt_carbon_intensity.estimationMethod() == "unknown");
    REQUIRE(qt_carbon_intensity.isForecast() == false);
    REQUIRE(qt_carbon_intensity.forecastMethod() == "unknown");
    REQUIRE(qt_carbon_intensity.isSynthetic() == false);
}