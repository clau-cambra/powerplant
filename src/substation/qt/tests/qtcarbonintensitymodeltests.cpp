//
// Created by Claudio Cambra on 27/12/24.
//

#include <QAbstractItemModelTester>
#include <QTest>

#include <substation/carbon-modelling/inmemorycarbonintensitystorage.h>
#include <substation/qt/carbonintensitymodel.h>

class QtCarbonIntensityModelTests : public QObject
{
    Q_OBJECT

private slots:
    void testCarbonIntensityModelEmpty() const
    {
        Substation::Qt::CarbonIntensityModel model;
        QAbstractItemModelTester modelTester(&model);

        QCOMPARE(model.columnCount(), Substation::Qt::CarbonIntensityModel::ColumnCount);
        QCOMPARE(model.rowCount(), 0);
    }

    void testCarbonIntensityModelAddingIntensities() const
    {
        const auto storage = std::make_shared<Substation::CarbonModelling::InMemoryCarbonIntensityStorage>();
        Substation::Qt::CarbonIntensityModel model(storage);
        QAbstractItemModelTester modelTester(&model);

        QCOMPARE(model.columnCount(), Substation::Qt::CarbonIntensityModel::ColumnCount);
        QCOMPARE(model.rowCount(), 0);

        const auto ci = Substation::CarbonModelling::CarbonIntensity{.zone = "ES",
                                                                     .intensity = 100,
                                                                     .datetime = 1640601600,
                                                                     .updated_at = 1640601600,
                                                                     .emission_factor_type = "unknown",
                                                                     .is_estimated = false,
                                                                     .estimation_method = "unknown",
                                                                     .is_forecast = false,
                                                                     .forecast_method = "unknown",
                                                                     .is_synthetic = false};
        storage->store(ci);
        QCOMPARE(model.rowCount(), 1);
    }

    void testCarbonIntensityModelData() const
    {
        const auto storage = std::make_shared<Substation::CarbonModelling::InMemoryCarbonIntensityStorage>();
        Substation::Qt::CarbonIntensityModel model(storage);
        QAbstractItemModelTester modelTester(&model);
        const auto ci = Substation::CarbonModelling::CarbonIntensity{.zone = "ES",
                                                                     .intensity = 100,
                                                                     .datetime = 1640601600,
                                                                     .updated_at = 1640601600,
                                                                     .emission_factor_type = "unknown",
                                                                     .is_estimated = false,
                                                                     .estimation_method = "unknown",
                                                                     .is_forecast = false,
                                                                     .forecast_method = "unknown",
                                                                     .is_synthetic = false};
        storage->store(ci);
        QCOMPARE(model.rowCount(), 1);

        const auto index = model.index(0, 0);
        QCOMPARE(model.data(index, Substation::Qt::CarbonIntensityModel::IntensityRole).toInt(), ci.intensity);
        QCOMPARE(model.data(index, Substation::Qt::CarbonIntensityModel::ZoneRole).toString(), ci.zone);
        QCOMPARE(model.data(index, Substation::Qt::CarbonIntensityModel::DateTimeRole).toDateTime(), QDateTime::fromSecsSinceEpoch(ci.datetime));
        QCOMPARE(model.data(index, Substation::Qt::CarbonIntensityModel::UpdatedAtRole).toDateTime(), QDateTime::fromSecsSinceEpoch(ci.updated_at));
        QCOMPARE(model.data(index, Substation::Qt::CarbonIntensityModel::EmissionFactorTypeRole).toString(), ci.emission_factor_type);
        QCOMPARE(model.data(index, Substation::Qt::CarbonIntensityModel::IsEstimatedRole).toBool(), ci.is_estimated);
        QCOMPARE(model.data(index, Substation::Qt::CarbonIntensityModel::EstimationMethodRole).toString(), ci.estimation_method);
        QCOMPARE(model.data(index, Substation::Qt::CarbonIntensityModel::IsForecastRole).toBool(), ci.is_forecast);
        QCOMPARE(model.data(index, Substation::Qt::CarbonIntensityModel::ForecastMethodRole).toString(),ci.forecast_method);
        QCOMPARE(model.data(index, Substation::Qt::CarbonIntensityModel::IsSyntheticRole).toBool(), ci.is_synthetic);

        const auto indexColumn1 = model.index(0, 1);
        QCOMPARE(model.data(indexColumn1).toDateTime(), QDateTime::fromSecsSinceEpoch(ci.datetime));
        const auto indexColumn2 = model.index(0, 2);
        QCOMPARE(model.data(indexColumn2).toInt(), ci.intensity);
    }
};

QTEST_GUILESS_MAIN(QtCarbonIntensityModelTests);
#include "qtcarbonintensitymodeltests.moc"