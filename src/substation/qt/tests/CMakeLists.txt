add_executable(qt-carbon-intensity-tests qtcarbonintensitytests.cpp)
target_link_libraries(qt-carbon-intensity-tests PRIVATE qt Catch2::Catch2WithMain)
add_test(NAME QtCarbonIntensityTests COMMAND qt-carbon-intensity-tests)

add_executable(qt-geolocation-tests qtgeolocationtests.cpp)
target_link_libraries(qt-geolocation-tests PRIVATE qt Catch2::Catch2WithMain)
add_test(NAME QtGeolocationTests COMMAND qt-geolocation-tests)

add_executable(qt-carbon-intensity-model-tests qtcarbonintensitymodeltests.cpp)
target_link_libraries(qt-carbon-intensity-model-tests PRIVATE qt Qt6::Test Catch2::Catch2WithMain)
add_test(NAME QtCarbonIntensityModelTests COMMAND qt-carbon-intensity-model-tests)

add_executable(qt-carbon-intensity-monitor-tests qtcarbonintensitymonitortests.cpp ../../carbon-modelling/tests/carbonmodellingtestutils.h)
target_link_libraries(qt-carbon-intensity-monitor-tests PRIVATE qt Catch2::Catch2WithMain)
add_test(NAME QtCarbonIntensityMonitorTests COMMAND qt-carbon-intensity-monitor-tests)