//
// Created by Claudio Cambra on 24/2/25.
//

#include <catch2/catch_all.hpp>

#include <substation/qt/geolocation.h>

TEST_CASE("Geolocation Qt wrapper returns expected values", "[geolocation-qt-values]")
{
    const auto geolocation = Substation::Geolocation::Geolocation{.name = "ES", .latitude = 40.4165, .longitude = -3.7026, .region = "Europe", .country = "Spain", .timezone = "CET"};
    const Substation::Qt::Geolocation qtGeolocation{geolocation};
    REQUIRE(qtGeolocation.name() == "ES");
    REQUIRE(qtGeolocation.latitude() == 40.4165f);
    REQUIRE(qtGeolocation.longitude() == -3.7026f);
    REQUIRE(qtGeolocation.region() == "Europe");
    REQUIRE(qtGeolocation.country() == "Spain");
    REQUIRE(qtGeolocation.timezone() == "CET");
}