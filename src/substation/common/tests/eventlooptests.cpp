//
// Created by Claudio Cambra on 16/2/25.
//

#include <catch2/catch_all.hpp>

#include <substation/common/event.h>
#include <substation/common/eventloop.h>

using namespace Substation::Common;

TEST_CASE("Event loop singleton is properly instantiated", "[event-loop-singleton]")
{
    REQUIRE(EventLoop::get_instance() != nullptr);
}

TEST_CASE("Event loop correctly executes immediate events", "[event-loop-execute]")
{
    auto completed = false;
    EventLoop::get_instance()->execute([&completed] {
        completed = true;
    });
    std::this_thread::sleep_for(std::chrono::milliseconds(1));
    REQUIRE(completed);
}

TEST_CASE("Event loop correctly executes events at a given time", "[event-loop-execute-at-time]")
{
    constexpr auto TEST_WAIT = std::chrono::milliseconds(1);
    const auto test_start = std::chrono::system_clock::now();
    const auto expected_completed = test_start + TEST_WAIT;
    auto completed = std::chrono::system_clock::time_point::min();
    EventLoop::get_instance()->add_event({expected_completed, [&completed] {
                                              completed = std::chrono::system_clock::now();
                                          }});
    std::this_thread::sleep_for(TEST_WAIT + std::chrono::milliseconds(1));
    REQUIRE(completed >= expected_completed);
    REQUIRE(completed < expected_completed + std::chrono::milliseconds(1));
}

TEST_CASE("Event loop correctly executes multiple events", "[event-loop-execute-multiple]")
{
    // Handle later task to be executed first to see if the expected order is kept
    constexpr auto TEST_WAIT_A = std::chrono::milliseconds(2);
    constexpr auto TEST_WAIT_B = std::chrono::milliseconds(1);

    const auto test_start = std::chrono::system_clock::now();
    const auto expected_completed_a = test_start + TEST_WAIT_A;
    const auto expected_completed_b = test_start + TEST_WAIT_B;
    auto completed_a = std::chrono::system_clock::time_point::min();
    auto completed_b = std::chrono::system_clock::time_point::min();

    EventLoop::get_instance()->add_event({expected_completed_a, [&completed_a] {
                                              completed_a = std::chrono::system_clock::now();
                                          }});
    EventLoop::get_instance()->add_event({expected_completed_b, [&completed_b] {
                                              completed_b = std::chrono::system_clock::now();
                                          }});

    std::this_thread::sleep_for(TEST_WAIT_A + std::chrono::milliseconds(1));
    REQUIRE(completed_a >= expected_completed_a);
    REQUIRE(completed_a < expected_completed_a + std::chrono::milliseconds(1));
    REQUIRE(completed_b >= expected_completed_b);
    REQUIRE(completed_b < expected_completed_b + std::chrono::milliseconds(1));
    REQUIRE(completed_a > completed_b);
}