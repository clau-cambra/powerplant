//
// Created by Claudio Cambra on 15/2/25.
//

#include "eventloop.h"

#include "event.h"

namespace Substation::Common
{

EventLoop *EventLoop::get_instance()
{
    static EventLoop event_loop_instance;
    return &event_loop_instance;
}

EventLoop::~EventLoop() noexcept
{
    m_running.store(false);
    m_condition_var.notify_one();
    m_thread.join();
}

void EventLoop::add_event(const Event &event)
{
    const std::scoped_lock<std::mutex> lock(m_event_queue_mutex);
    m_write_queue.push(event);
    m_condition_var.notify_one();
}

void EventLoop::execute(const std::function<void()> &function)
{
    add_event({std::chrono::system_clock::now(), function});
}

void EventLoop::event_loop()
{
    while (m_running) {
        std::unique_lock lock(m_event_queue_mutex);
        const auto next_timeout = m_write_queue.empty() ? std::chrono::system_clock::time_point::max() : m_write_queue.top().timeout;
        m_condition_var.wait_until(lock, next_timeout);

        if (!m_running) {
            break;
        } else if (m_write_queue.empty() || std::chrono::system_clock::now() < m_write_queue.top().timeout) {
            continue;
        }

        const auto event = m_write_queue.top();
        m_write_queue.pop();
        lock.unlock();
        event.function();
    }
}

}