//
// Created by Claudio Cambra on 15/2/25.
//

#include <atomic>
#include <functional>
#include <mutex>
#include <thread>

namespace Substation::Common
{

struct Event;

class EventLoop
{
public:
    static EventLoop *get_instance();
    ~EventLoop() noexcept;

    void add_event(const Event &event);
    void execute(const std::function<void()> &function);

private:
    explicit EventLoop() = default;

    void event_loop();

    std::thread m_thread{&EventLoop::event_loop, this};
    std::mutex m_event_queue_mutex;
    std::priority_queue<Event, std::vector<Event>, std::greater<>> m_write_queue;
    std::atomic_bool m_running = true;
    std::condition_variable m_condition_var;
};

}