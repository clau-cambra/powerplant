//
// Created by Claudio Cambra on 28/12/24.
//

#include <iostream>
#include <print>
#include <sys/types.h>
#include <unistd.h>

#include <substation/geolocation/ipinfogeolocationinterface.h>
#include <substation/geolocation/ipinfogeolocationinterface.h>
#include <substation/carbon-modelling/carbonintensitymonitor.h>
#include <substation/carbon-modelling/electricitymapscarbonintensityinterface.h>
#include <substation/carbon-modelling/filebasedcarbonintensitystorage.h>

int main(int argc, char *argv[])
{
    // Get ip info token and electricity maps api token from args
    if (argc < 3) {
        std::cerr << "Usage: " << argv[0] << " <ip_info_token> <electricity_maps_api_token>" << std::endl;
        return 1;
    }
    const auto ip_info_token = std::string(argv[1]);
    const auto electricity_maps_api_token = std::string(argv[2]);

    const auto geolocation_interface = std::make_shared<Substation::Geolocation::IpInfoGeolocationInterface>(ip_info_token);
    const auto carbon_intensity_interface = std::make_shared<Substation::CarbonModelling::ElectricityMapCarbonIntensityInterface>(electricity_maps_api_token);
    const auto carbon_intensity =
        Substation::CarbonModelling::CarbonIntensityMonitor::create(geolocation_interface, carbon_intensity_interface)->fetch_carbon_intensity();
    if (!carbon_intensity.has_value()) {
        return 1;
    }
    Substation::CarbonModelling::FileBasedCarbonIntensityStorage().store(*carbon_intensity);

    // Clean up old forecast files

    return 0;
}
