//
// Created by Claudio Cambra on 9/3/25.
//

#include <chrono>
#include <substation/carbon-minimisation/taskscheduler.h>

#include "../benchmarkutils.h"

int main(int argc, char *argv[])
{
    constexpr auto NAME = "delayed";
    Substation::BenchmarkUtils::Benchmark benchmark(argc, argv, NAME);

    const auto results_path = benchmark.results_store_path();
    const auto curve = benchmark.curve();
    auto start_time = std::chrono::system_clock::time_point::min();

    std::binary_semaphore semaphore(0);
    const auto test_iter = [&semaphore, &benchmark, &start_time, &results_path] {
        [[unlikely]] if (start_time == std::chrono::system_clock::time_point::min()) {
            start_time = std::chrono::system_clock::now();
        }
        const auto iteration_opt = benchmark.benchmark_iteration(semaphore);
        if (iteration_opt) {
            const auto iteration = *iteration_opt;
            if (iteration % 100 == 0) {
                const auto remaining_iters = benchmark.test_iterations() - iteration;
                const auto elapsed_time = std::chrono::duration_cast<std::chrono::seconds>(std::chrono::system_clock::now() - start_time);
                const auto expected_duration = elapsed_time * remaining_iters / iteration;
                spdlog::info("Expected remaining duration: {} seconds", expected_duration.count());
                Substation::BenchmarkUtils::store_test_intensity_to_file(results_path, benchmark.curve(), iteration);
            }
        }
        return iteration_opt;
    };
    const auto scheduler = Substation::CarbonMinimisation::Execution::TaskScheduler::get_instance();
    scheduler->set_carbon_curve(curve);
    scheduler->schedule_task(test_iter, std::chrono::hours(24));
    semaphore.acquire();
}