//
// Created by Claudio Cambra on 2/3/25.
//

#include <chrono>

#include <substation/carbon-minimisation/taskmonitor.h>
#include <substation/carbon-minimisation/throttledescriptor.h>

#include "../benchmarkutils.h"

int main(int argc, char *argv[])
{
    constexpr auto NAME = "throttled";
    Substation::BenchmarkUtils::Benchmark benchmark(argc, argv, NAME);

    const auto results_path = benchmark.results_store_path();
    const auto curve = benchmark.curve();

    // Throttled execution
    {
        std::binary_semaphore semaphore(0);
        // A note on the min/max cpu values; these are adjusted to precent exec taking longer than 24 hours
        const auto throttle = Substation::CarbonMinimisation::Execution::ThrottleDescriptor{100, 50};
        const auto task_desc = Substation::CarbonMinimisation::Execution::IteratingTaskDescriptor{
            .expected_iters = benchmark.test_iterations(),
            .task_iter =
                [&benchmark, &semaphore] {
                    return benchmark.benchmark_iteration(semaphore);
                },
            .throttle_desc = throttle,
            .run_within = std::chrono::seconds::zero(),
        };
        float current_limit = 100.0;

        auto monitor = Substation::CarbonMinimisation::Execution::TaskMonitor(task_desc, curve, [](const auto seconds) {
            spdlog::info("Expected remaining duration: {} seconds", seconds.count());
        });
        monitor.set_applied_cpu_limit_callback([&current_limit](const float limit) {
            current_limit = limit;
        });
        monitor.set_iteration_completed_callback([&results_path, &curve, &current_limit](const int monitor_iteration) {
            Substation::BenchmarkUtils::store_test_intensity_to_file(results_path, curve, monitor_iteration, current_limit);
        });
        monitor.start();
        semaphore.acquire();
    }
}