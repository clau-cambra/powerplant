//
// Created by Claudio Cambra on 8/3/25.
//

#include <chrono>
#include <filesystem>
#include <semaphore>

#include <QCommandLineParser>
#include <QCoreApplication>

#include <substation/carbon-minimisation/taskmonitor.h>
#include <substation/carbon-minimisation/throttledescriptor.h>
#include <substation/carbon-modelling/filebasedcarbonintensitystorage.h>

#include "../benchmarkutils.h"

int main(int argc, char *argv[])
{
    constexpr auto NAME = "standard";
    Substation::BenchmarkUtils::Benchmark benchmark(argc, argv, NAME);
    // This is a synthetic benchmark that simulates a task that consumes a lot of power.
    // Without throttling, this takes ~12 hours on an M1 Macbook Air.
    std::binary_semaphore semaphore(0);
    const auto results_path = benchmark.results_store_path();

    // Standard execution
    {
        const auto start_time = std::chrono::system_clock::now();
        while (const auto iteration_opt = benchmark.benchmark_iteration(semaphore)) {
            // Calculate remaining time based on completed iterations
            const auto iteration = *iteration_opt;
            if (iteration % 100 == 0) {
                const auto remaining_iters = benchmark.test_iterations() - iteration;
                const auto elapsed_time = std::chrono::duration_cast<std::chrono::seconds>(std::chrono::system_clock::now() - start_time);
                const auto expected_duration = elapsed_time * remaining_iters / iteration;
                spdlog::info("Expected remaining duration: {} seconds", expected_duration.count());
                Substation::BenchmarkUtils::store_test_intensity_to_file(results_path, benchmark.curve(), iteration);
            }
        }
    }
}