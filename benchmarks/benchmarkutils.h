//
// Created by Claudio Cambra on 8/3/25.
//

#include "substation/carbon-modelling/carbonintensity.h"
#include <QCommandLineParser>
#include <QCoreApplication>

#include <semaphore>
#include <spdlog/spdlog.h>

#include <substation/carbon-modelling/carbonintensitycurve.h>
#include <substation/carbon-modelling/filebasedcarbonintensitystorage.h>

namespace Substation::BenchmarkUtils
{

constexpr auto DEFAULT_ITERATIONS = 2000000;

void verify_test_data(const std::vector<CarbonModelling::CarbonIntensity> &intensities)
{
    assert(intensities.size() >= 97);
}

std::vector<CarbonModelling::CarbonIntensity> data_with_shifted_times(const std::vector<CarbonModelling::CarbonIntensity> &intensities)
{
    using namespace std::chrono;
    const auto now = system_clock::to_time_t(system_clock::now());
    auto sorted_intensities = intensities;
    std::ranges::sort(sorted_intensities, std::less{}, [](const auto &ci) {
        return ci.datetime;
    });
    const auto first_time = sorted_intensities.front().datetime;
    std::vector<Substation::CarbonModelling::CarbonIntensity> shifted_intensities;
    std::ranges::transform(sorted_intensities, std::back_inserter(shifted_intensities), [first_time, now](auto intensity) {
        intensity.datetime -= first_time;
        intensity.datetime += now;
        return intensity;
    });
    return shifted_intensities;
}

void store_test_intensity_to_file(const std::filesystem::path &base_path,
                                  const std::shared_ptr<Substation::CarbonModelling::CarbonIntensityCurve> &curve,
                                  const int iteration,
                                  const float cpu_limit = 100.0)
{
    if (iteration % 100 != 0) {
        return;
    }
    const auto filename = base_path / std::format("ci_{}_{}.json", iteration, cpu_limit);
    const auto intensity = curve->current_intensity(Substation::CarbonModelling::CarbonIntensityCurve::none, false);
    assert(intensity.has_value());
    assert((*intensity).write_to_file(filename));
    spdlog::info("Wrote carbon intensity to file: {}", filename.string());
}

bool data_store_path_available(const std::filesystem::path &path)
{
    return std::filesystem::exists(path) || std::filesystem::create_directories(path);
}

class Benchmark
{
public:
    explicit Benchmark(int argc, char *argv[], const std::string &benchmark_name)
    {
        QCoreApplication app(argc, argv);

        QCommandLineParser parser;
        QCommandLineOption test_data_path_option("test-data-path", "Path to the test data", "path");
        QCommandLineOption results_store_path_option("results-store-path", "Path to store benchmark data", "path");
        QCommandLineOption test_iterations_option("iterations", "Number of test task iterations to perform", "int");

        parser.addOptions({test_data_path_option, results_store_path_option, test_iterations_option});
        parser.process(QCoreApplication::arguments());

        m_synthetic_data_path = parser.value(test_data_path_option).toStdString();
        if (m_synthetic_data_path.empty()) {
            spdlog::error("No test data path provided.");
            throw;
        }
        if (!std::filesystem::exists(m_synthetic_data_path)) {
            spdlog::error("Test data path does not exist.");
            throw;
        }
        const auto base_results_store_path = std::filesystem::path(parser.value(results_store_path_option).toStdString());
        if (base_results_store_path.empty()) {
            spdlog::error("No benchmark data store path provided.");
            throw;
        }
        m_results_store_path = base_results_store_path / benchmark_name;
        if (!Substation::BenchmarkUtils::data_store_path_available(m_results_store_path)) {
            spdlog::error("Benchmark data store path does not exist.");
            throw;
        }
        m_test_iterations = parser.value(test_iterations_option).toInt();
        if (m_test_iterations <= 0) {
            spdlog::error("Invalid number of test iterations provided, setting default.");
            m_test_iterations = Substation::BenchmarkUtils::DEFAULT_ITERATIONS;
        }

        const Substation::CarbonModelling::FileBasedCarbonIntensityStorage storage(m_synthetic_data_path);
        const auto synthetic_data = *(storage.data());
        Substation::BenchmarkUtils::verify_test_data(synthetic_data);
        spdlog::info("Loaded in {} carbon intensity data points.", synthetic_data.size());
        m_benchmark_intensity_data = data_with_shifted_times(synthetic_data);
        m_curve = std::make_shared<Substation::CarbonModelling::CarbonIntensityCurve>(m_benchmark_intensity_data);

        spdlog::set_level(spdlog::level::debug);
    }

    std::optional<size_t> benchmark_iteration(std::binary_semaphore &semaphore)
    {
        // An intense workload that consumes a lot of power.
        auto result = 1.0;
        for (auto i = 0; i < m_test_iterations; i++) {
            result = std::sqrt(result * result + 1.0);
            if (result > 10000.0) {
                result = 1.0;
            }
        }
        if (m_current_iteration >= m_test_iterations) {
            semaphore.release();
            return std::nullopt;
        }
        return ++m_current_iteration;
    }

    size_t test_iterations() const
    {
        return m_test_iterations;
    }

    size_t current_iteration() const
    {
        return m_current_iteration;
    }

    std::filesystem::path results_store_path() const
    {
        return m_results_store_path;
    }

    std::filesystem::path synthetic_data_path() const
    {
        return m_synthetic_data_path;
    }

    std::vector<CarbonModelling::CarbonIntensity> benchmark_intensity_data() const
    {
        return m_benchmark_intensity_data;
    }

    std::shared_ptr<CarbonModelling::CarbonIntensityCurve> curve() const
    {
        return m_curve;
    }

private:
    size_t m_test_iterations = 0;
    size_t m_current_iteration = 0;
    std::filesystem::path m_results_store_path;
    std::filesystem::path m_synthetic_data_path;
    std::vector<CarbonModelling::CarbonIntensity> m_benchmark_intensity_data;
    std::shared_ptr<CarbonModelling::CarbonIntensityCurve> m_curve;
};

}